/*
 *  AMSAnalyser.cxx
 *  
 *
 *  Created by Gillard on 25/05/11.
 *  Copyright 2011 L.P.S.C. All rights reserved.
 *
 */

#include "AMSAnalyser.h"
#include <LxTime.h>
#include <LxAMSdata.h>
#include <LxAMSdataManager.h>
#include <LxDATAtools.h>

using namespace std;


int CleanLOG(const char *_fName)
{
	std::fstream haystack (_fName, std::fstream::in);
	
	if(!haystack.is_open())
	{
		printf("   File %s not found\n\n",_fName);
		return 1;
	}
	
	std::string needle = "<< Success";
	std::string line;
	int pos;
	bool found = false;
	
	// Search for needle
	while ( !found && getline (haystack,line) )
	{
		pos = line.find(needle);  // find position of needle in current line
		if (pos != std::string::npos)
			found = true;
	}
	
	haystack.close();
	if(found && !remove(_fName))
		printf("••••• Cleaning Logfile %s •••••\n\n",_fName);
	else
		printf("   Can't find '<< Success' string in %s\n\n",_fName);
		
	return found;
}

//-------------------------------------- THE MAIN PROGRAM --------------------------------------//
//----------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------//
int main(int argc, char **argv)
{
	
  //gSystem->SetAclicMode(TSystem::kDebug);
	
  //gSystem->SetFlagsDebug("-Wno-unknown-pragmas -g");
  
  TString cmd= gSystem->GetMakeSharedLib(); 
  cmd.ReplaceAll("g++","g++ -pthread"); 
  gSystem->SetMakeSharedLib(cmd);

	TOption Param=TOption();
	if(!Param.HandleInputPar(argc,argv)) return 0;
	
	char *AMSWD;
	char includePath[500];
	AMSWD=getenv("AMSWD");
	if(AMSWD != NULL)
	{
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("$HOME/include"));
		gSystem->SetIncludePath(includePath);
		printf("   \033[22;34m- Include Path :\033[0m %s\n",includePath);
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("$CRSIMDROOT/geomag"));
		gSystem->AddIncludePath(includePath);
		printf("   \033[22;34m- Include Path :\033[0m %s\n",includePath);
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("./"));
		gSystem->AddIncludePath(includePath);
		printf("   \033[22;34m- Include Path :\033[0m %s\n",includePath);
		strcpy(includePath,"-I");
		//strcat(includePath,gSystem->ExpandPathName("$AMSWD/AMS/include"));
		strcat(includePath,gSystem->ExpandPathName("$AMSWD/include"));
		gSystem->AddIncludePath(includePath);
		printf("   \033[22;34m- Include Path :\033[0m %s\n",includePath);
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("$LXSOFT/LxAMS/LxBase"));
		gSystem->AddIncludePath(includePath);
		printf("   \033[22;34m- Include Path :\033[0m %s\n",includePath);
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("$LXSOFT/LxAMS/LxAna"));
		gSystem->AddIncludePath(includePath);
		printf("   \033[22;34m- Include Path :\033[0m %s\n",includePath);
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("$LXSOFT/LxAMS/LxGeomag"));
		gSystem->AddIncludePath(includePath);
		printf("   \033[22;34m- Include Path :\033[0m %s\n",includePath);
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("$LXSOFT/LxAMS/LxSel"));
		gSystem->AddIncludePath(includePath);
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("$LXSOFT/ASTROLIB/include"));
		gSystem->AddIncludePath(includePath);
		strcpy(includePath,"-I");
		//strcat(includePath,"/usr/include/cfitsio");
		strcat(includePath,gSystem->ExpandPathName("$HOME/CFITSIO/include"));
		gSystem->AddIncludePath(includePath);
		strcpy(includePath,"-I");
		strcat(includePath,gSystem->ExpandPathName("$LXSOFT/LxAMS/LxTools"));
		gSystem->AddIncludePath(includePath);
		printf("   \033[22;34m- Include Path :\033[0m %s\n",includePath);
		gSystem->Load(gSystem->ExpandPathName("$CRSIMDROOT/LIBS/libCRgeomag.so"));
		gSystem->Load(gSystem->ExpandPathName("$LXSOFT/LxAMS/LIBS/$AMS_VER/libLipRec_.so"));
		gSystem->Load(gSystem->ExpandPathName("$LXSOFT/LxAMS/LIBS/$AMS_VER/libLxGeomag_.so"));
		gSystem->Load(gSystem->ExpandPathName("$LXSOFT/LxAMS/LIBS/$AMS_VER/libLxBase_.so"));
		gSystem->Load(gSystem->ExpandPathName("$LXSOFT/LxAMS/LIBS/$AMS_VER/libLxAna_.so"));
		gSystem->Load(gSystem->ExpandPathName("$LXSOFT/LxAMS/LIBS/$AMS_VER/libLxSolarmod_.so"));
		gSystem->Load(gSystem->ExpandPathName("$LXSOFT/LxAMS/LIBS/$AMS_VER/libLxTreePlayer.so"));
		gSystem->Load(gSystem->ExpandPathName("$LXSOFT/LxAMS/LIBS/$AMS_VER/libLxTreeUpdate.so"));
		gSystem->Load(gSystem->ExpandPathName("$LXSOFT/ASTROLIB/LIB/libAstro_.so"));
		gSystem->Load(gSystem->ExpandPathName("$HOME/CFITSIO/lib64/libcfitsio.so"));
		//gSystem->Load(gSystem->ExpandPathName("$AMSWD/lib/linuxx8664gcc/ntuple_slc4_PG.so")); 
		//gSystem->Load("/usr/lib64/libpthread.so");
		//gSystem->Load(gSystem->ExpandPathName("./libComputeExpTime.so"));
		//gSystem->Load(gSystem->ExpandPathName("./libComputeRTIExpTime.so"));
	}
	else return 0;
	
	TkDBc::UseFinal();
	
	gSystem->Setenv("TFile::Recover","0");
	gSystem->ResetErrno();
	
	// Case of Merging file : No analyses performed.
	if(Param.merge) return Param.Merge();
	
	AMSChain* chain = new AMSChain();
//	chain->SetAutoDelete(kTRUE);
//	chain->CanDeleteRefs(kTRUE);
	
	// START CPU BENCHMARKING AND MEMORY STAT
	TBenchmark *CPU = new TBenchmark();
	//TBenchmark *FileCheck = new TBenchmark();

	CPU->Start("FileCheck");
	vector<TString> AddedFile;
	
	TString CCDir=Param.InputFile[0];
	if(CCDir.Contains("ISS"))
	{
		while(CCDir.Contains("ISS"))
			CCDir.Remove(0,CCDir.First(".")+1);
		CCDir.Remove(CCDir.First("/"),CCDir.Length()-(CCDir.First("/")));
	}
	if(CCDir.Contains("TB"))
	{
		CCDir="TB";
	}
	else if(CCDir.Contains("/ams/mc/"))
		CCDir="MC";
	
	printf("\033[24;31mDATA DIR : %s\033[0m\n",CCDir.Data());
	
	
	LxAMSdataManager::badrunfile=gSystem->ExpandPathName(Form("/sps/ams/AMSDataDir/CCALI_BadFile/CCALI_%s.BadRunList.txt",CCDir.Data()));
	LxAMSdata *pData= new LxAMSdata();
	pData->SetBadRunFile(LxAMSdataManager::badrunfile);
	
		
	vector<string> DataFiles;
	vector<string> goodFile; 
	
	// Case file to be analyse are specified
	if(!Param.STARTAT && !Param.ENDAT)
	{
		for(unsigned int i=0; i<Param.InputFile.size(); i++)
		{
			TString FileName = Param.InputFile[i];
			TString DataAccess;
			if(FileName.Contains(".root"))
			{
				if(FileName.Contains("root://ccxroot:1999//hpss/in2p3.fr/group"))
				{	
					FileName.Remove(0,40);
					FileName.ReplaceAll("//","/");
					DataAccess=FileName;
					DataAccess.Resize(DataAccess.Last('/'));
				
					pData =  LxAMSdataManager::AccessData("none",DataAccess.Data());//FileName.Data());
					pData->SetBadRunFile(LxAMSdataManager::badrunfile);
//					pData->ValidateRootFile(string(FileName.Data()));
					
					if(Param.Check)
					{
						if(!pData->ValidateRootFile(FileName.Data()))
							continue;
					}					
					
					DataFiles.push_back(FileName.Data());
				}
				else
				{
					FileName.ReplaceAll("//","/");
					DataAccess=FileName;
					DataAccess.Resize(DataAccess.Last('/'));
				
					pData =  LxAMSdataManager::AccessData(Param.DataTag,DataAccess.Data());//FileName.Data());
					pData->SetBadRunFile(LxAMSdataManager::badrunfile);
					pData->GetFiles(pData->GetRun(FileName.Data()),"NOCHECK");
					
					if(Param.Check)
					{
						if(!pData->ValidateRootFile(FileName.Data()))
							continue;
					}
				
					string ROOTfilename = FileName.Data();
					if(!FileName.Contains(pData->GetRootDirPrefix()))
						ROOTfilename = pData->GetRootDirPrefix() + FileName.Data();
					
					DataFiles.push_back(ROOTfilename.c_str());
				}
			}
			else
			{
				if(FileName.Contains("root://ccxroot:1999//hpss/in2p3.fr/group"))
				{	
					FileName.Remove(0,40);
					FileName.ReplaceAll("//","/");
				}
				printf("\033[34m DataFile : %s\033[0m\n",FileName.Data());
				
				pData =  LxAMSdataManager::AccessData(Param.DataTag,FileName.Data());
				pData->SetBadRunFile(LxAMSdataManager::badrunfile);
				
				if(Param.Check)
					goodFile = pData->GetFiles("CHECK");
				else
					goodFile = pData->GetFiles();
				
				
				for(unsigned int iF=0; iF<goodFile.size(); iF++)
					DataFiles.push_back(goodFile[iF]);
				
				if(goodFile.size() > 0)
					goodFile.clear();				
			}
		}

		for(unsigned int i=0; i<DataFiles.size(); i++)
		{
			string ROOTfilename = pData->GetRootDirPrefix() + DataFiles[i];
			
			string RunID = ROOTfilename.substr(ROOTfilename.find_last_of("/")+1,ROOTfilename.length()-ROOTfilename.find_last_of("/"));
			RunID        = RunID.substr(0,RunID.find_first_of("."));
			unsigned int RUN=(unsigned int) atoi(RunID.c_str());
			
			//			if(LxDATAtools::getInstance("/afs/in2p3.fr/group/ams/Offline/AMSDataDir/BadRuns/")->IsBadRun(RUN))
			//{
			//	printf("    File [%i] \033[22;34m%s\033[31m LISTED AS ISS BADRUN\033[0m.\n",i+1,ROOTfilename.c_str());
			//	continue;
			//}
			
			if(chain->Add(ROOTfilename.c_str()) <= 0)
			{
				printf("\033[43;31m!!!! ERROR !!!!\033[0m\n    Can not add file \033[22;31m%s\033[0m.\n",ROOTfilename.c_str());
				continue;
			}
			else
			{
				AddedFile.push_back(TString(ROOTfilename));
			}
			
		
			printf("    Add file [%i] \033[22;34m%s\033[0m to chain.\n",i+1,ROOTfilename.c_str());
		}
	}
	else
	{
		bool MATCH;
		for(unsigned int i=0; i<Param.InputFile.size(); i++)
		{
			if(strstr(Param.InputFile[i],".root")) 
			{
				printf("\033[43;31m!!!! ERROR !!!!\033[0m\n    %s\n    Range Date option only require data directory.\n",Param.InputFile[i]);
				continue;
			}
			pData =  LxAMSdataManager::AccessData("",Param.InputFile[i]);
			pData->SetBadRunFile(LxAMSdataManager::badrunfile);
			
			if(Param.Check)
				DataFiles = pData->GetFiles(LxTime::GetTime(Param.STARTAT),LxTime::GetTime(Param.ENDAT),"CHECK");
			else
				DataFiles = pData->GetFiles(LxTime::GetTime(Param.STARTAT),LxTime::GetTime(Param.ENDAT));
			
			if(DataFiles.size() <= 0) continue;
			
			for (unsigned int k=0; k<DataFiles.size(); k++)
			{
				if(DataFiles[k].length() == 0)
					continue;
				
				MATCH=false;
				
				// CHECK THAT THE RUN HAVE NOT YET BEEN INCLUDED
				if(AddedFile.size() != 0) 
				{
					TString RUN=DataFiles[k];
					RUN.Remove(0,RUN.Last('/')+1);
					for(unsigned int TESTOR=0; TESTOR<AddedFile.size(); TESTOR++)
					{
							if(AddedFile[TESTOR].Contains(RUN.Data()))
										{MATCH=true; printf("\033[22;31m     ---> RUN %s already included as %s\033[0m\n",RUN.Data(),AddedFile[TESTOR].Data());}
					}
				}

				if(MATCH) continue;
				
				// IF FILE NOT ALREADY IN THE LIST, ADD TO THE CHAIN
				string ROOTfilename = pData->GetRootDirPrefix() + DataFiles[k];
				
				string RunID = ROOTfilename.substr(ROOTfilename.find_last_of("/")+1,ROOTfilename.length()-ROOTfilename.find_last_of("/"));
				RunID        = RunID.substr(0,RunID.find_first_of("."));
				unsigned int RUN=(unsigned int) atoi(RunID.c_str());
				
//				if(LxDATAtools::getInstance("/afs/in2p3.fr/group/ams/Offline/AMSDataDir/BadRuns/")->IsBadRun(RUN))
//				{
//					printf("    File [%i] \033[22;34m%s\033[31m LISTED AS ISS BADRUN\033[0m.\n",i+1,ROOTfilename.c_str());
//					continue;
//				}
				
				if(chain->Add(ROOTfilename.c_str()) <=0)
				{
					printf("\033[43;31m!!!! ERROR !!!!\033[0m\n    Can not add file \033[22;31m%s\033[0m to chain.\n",ROOTfilename.c_str());
					continue;
				}
				printf("    Add file \033[22;34m%s\033[0m to chain.\n",ROOTfilename.c_str());
				AddedFile.push_back(TString(ROOTfilename));
			}
			DataFiles.clear();
		}
		
	}
	
	int FirstRun, LastRun;
	TString RUNTAG;
	if(AddedFile.size() <= 0)
	{
		AddedFile.clear();
		CPU->Stop("FileCheck");
		printf("\n\n\033[22;31m-----------------------------\n\033[22;34mBENCHMARK\033[0m.\n");
		CPU->Show("FileCheck");
		printf("\033[22;31m-----------------------------\033[0m\n\n\n");
		delete CPU;
		return AddedFile.size();
	}
	else
	{
		// SORT THE VECTOR BY INCREASING RUN TAG
		sort(AddedFile.begin(), AddedFile.end());
		RUNTAG = AddedFile[0];
		FirstRun = TString(RUNTAG(RUNTAG.Last('/')+1,RUNTAG.First('.')-1)).Atoi();
		
		RUNTAG = AddedFile[AddedFile.size()-1];
		LastRun = TString(RUNTAG(RUNTAG.Last('/')+1,RUNTAG.First('.')-1)).Atoi();
		strcat(Param.OPTIONS,Form(" -date %i %i ",FirstRun,LastRun));
		
		AddedFile.clear();
	}
	
	delete pData;
	if(Param.testmode)
		return -128;
	
	printf("    Total number of event : \033[22;34m%lli\033[0m.\n",(Long64_t) chain->GetEntries());
	if((Long64_t) chain->GetEntries() == 0)
		return 0;
	
	CPU->Stop("FileCheck");
	printf("\n\n\033[22;31m-----------------------------\n\033[22;34mBENCHMARK\033[0m.\n");
	CPU->Show("FileCheck");


/// ------ BEGIN ANALYSES -------	
	
	CPU->Start("CPU");
	
	TString LogFile;
	if(Param.OutputROOT != NULL)
		LogFile= Form("./%s",Param.OutputROOT);
	else
		LogFile= Form("./%s",Param.Script);
	
	LogFile.ReplaceAll(".root","");
	LogFile.ReplaceAll(".C+","");
	LogFile.Append(".LOG");
	int OUTfd, ERRfd;
	fpos_t OUTpos, ERRpos;
	
	if(!Param.stdoutput)
	{
		printf("Redirect stdout and stderr to %s\n\n",LogFile.Data());
		
		fflush(stdout);							//<< All remaining data in buffer get send to stdout
		fgetpos(stdout, &OUTpos);				//<< Store current position in stdout
		OUTfd = dup(fileno(stdout));			//<< Clone file description of what is currently stdout
		freopen (LogFile.Data(),"a",stdout);	//<< Redirect std out to a file
		
		fflush(stderr);							//<< All remaining data in buffer get send to stdout
		fgetpos(stderr, &ERRpos);				//<< Store current position in stdout
		ERRfd = dup(fileno(stderr));			//<< Clone file description of what is currently stdout
		freopen (LogFile.Data(),"a",stderr);
	}

	chain->SetCacheLearnEntries(1000);
	chain->SetParallelUnzip(kTRUE);
	
	if(Param.nentries <= 0 && Param.FirstEntry == 0) 
	{
	   printf("%s\n",Param.Script);
		//chain->TChain::Process(Param.Script,Param.OPTIONS);
		chain->Process(Param.Script,Param.OPTIONS);
	}
	else
	{
	   if(Param.nentries <= 0) Param.nentries=chain->GetEntries(); 
	   //chain->TChain::Process(Param.Script,Param.OPTIONS,Param.nentries,Param.FirstEntry);
		chain->Process(Param.Script,Param.OPTIONS,Param.nentries,Param.FirstEntry);
	}
	
	if(!Param.stdoutput)
	{
		fflush(stdout);					//<< All remaining data in buffer get send to stdout
		dup2(OUTfd,fileno(stdout));		//<< we clone the original stdout over the current stdout
		close (OUTfd);					//<< we close the cloned file
		fsetpos(stdout, &OUTpos);			//<< we restore our position in stdout
		
		fflush(stderr);					//<< All remaining data in buffer get send to stdout
		dup2(ERRfd,fileno(stderr));		//<< we clone the original stdout over the current stdout
		close (ERRfd);
		fsetpos(stderr, &ERRpos);		//<< we restore our position in stdout//<< we restore our position in stdout
	}
	
/// ------ IN CASE THE CLONETREE OPTION IS ACTIVATED -------
	TString TheFileName;
	if(strstr(Param.OPTIONS,".root") != NULL)
	{
		TheFileName = Param.OPTIONS;
		TheFileName.Remove(TheFileName.Index(".root")+5,TheFileName.Length()-(TheFileName.Index(".root"))+5);
		int kTest=0;
		while(TheFileName.Contains(" "))
		{
			TheFileName.Remove(0,TheFileName.Index(" ")+1);
			kTest++;
			if(kTest > 10000)
			{
				printf("\033[43;31m!!! ERROR IN EXTRACTING INPUT FILE NAME\033[0m\n      Set Default File name.\n");
				TheFileName="output.root";
				break;
			}
		}
	}
	else TheFileName="output.root";
	
	if(!TheFileName.Contains(".root")) TheFileName="output.root";
	
	TFile *outft = new TFile(TheFileName.Data(),"UPDATE");
	chain->Rewind();
	
	if(strstr(Param.OPTIONS,"CloneTree") !=NULL)
	{
		TObjString* obj2=(TObjString*)chain->GetFile()->Get("AMS02Geometry");
		if(obj2) {outft->cd();obj2->Write("AMS02Geometry");}
	}
	
	TObjString* obj3=(TObjString*)chain->GetFile()->Get("DataCards");
	if(obj3) {outft->cd();obj3->Write("DataCards");}
	
	outft->Close();
	delete outft;

/// ------ TERMINATE -------
	delete chain;
	
	
	printf("\n\n\033[22;31m-----------------------------\n\033[22;34mBENCHMARK\033[0m.\n");
	CPU->Show("FileCheck");
	CPU->Show("CPU");
	printf("\033[22;31m-----------------------------\033[0m\n\n\n");
	delete CPU;
	
	int status = 1;
	
	if(!Param.stdoutput)
		status = CleanLOG(LogFile.Data());
	if(status = 1)
		remove(LogFile.Data());
	
	printf("\033[22;31m     ***** AMSAnalyser ended successfully *****\033[0m\n\n\n");
	return status;
}
