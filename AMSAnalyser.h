#ifndef _PGTRACK_
#define _PGTRACK_
#endif

#include <root.h>
#include <TrCalDB.h>
#include <amschain.h>
#include <TSystem.h>
#include <stdio.h>
#include <string.h>
#include <TFile.h>
//#include <TRFIOFile.h>
#include <TBasket.h>
#include <TBenchmark.h>
#include <TMemStat.h>
#include <signal.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TObject.h>
#include <TKey.h>
#include <TString.h>
#include <dirent.h>
#include <fstream>

bool TestFile(char* FileName)
{
	//signal(SIGABRT,SIG_DFL);
	
	//CHECK THE FILE ISN'T CORRUPTED
	
	if(!strcmp(FileName,"root://ccxroot:1999//hpss/in2p3.fr/group/ams/data/2011B/ISS.B530/pass2/1311221993.00000001.root")) return false;
	if(!strcmp(FileName,"root://ccxroot:1999//hpss/in2p3.fr/group/ams/data/2011B/ISS.B530/pass2/1311228882.00000001.root")) return false;
	if(!strcmp(FileName,"root://ccxroot:1999//hpss/in2p3.fr/group/ams/data/2011B/ISS.B530/pass2/1310910754.00000001.root")) return false;
	
	TFile *File = new TFile();
	File->SetOpenTimeout(60000);
	File = TFile::Open (FileName,"READ");
	
	gSystem->ResetErrno();
	
	if (File == 0x0)
	{
		printf("\033[43;31m!!!! ERROR !!!!\033[0m\n      Bad File \033[22;31m%s\033[0m Access\n\n",FileName);
		return false;
	}
	
	if (File->IsZombie())
	{
		printf("\033[43;31m!!!! ERROR !!!!\033[0m\n      File \033[22;31m%s\033[0m is zombie\n\n",FileName);
		File->Close();
		return false;
	}
	
	if (File->TestBit(TFile::kRecovered))
	{
		printf("\033[43;31m!!!! ERROR !!!!\033[0m\n      File \033[22;31m%s\033[0m has been recovered\n\n",FileName);
		File->Close();
		return false;
	}
	
	gSystem->ResetErrno();
	TDirectory *datacards;
	try {datacards = (TDirectory*) File->Get("datacards");}
	catch(bad_alloc)
	{
		printf("\033[43;31m!!!! ERROR !!!!\033[0m\n      Catch bad_alloc : File \033[22;31m%s\033[0m has been corrupted\n\n",FileName);
		File->Close();
		delete File;
		return false; 
	}
	catch(...)
	{
		printf("\033[43;31m!!!! ERROR !!!!\033[0m\n      Catch bad_alloc2 : File \033[22;31m%s\033[0m has been corrupted\n\n",FileName);
		File->Close();
		delete File;
		return false; 
	}
	
	if(gSystem->GetErrno() > 0)
	{
		printf("\033[43;31m!!!! ERROR !!!!\033[0m\n      File \033[22;31m%s\033[0m has been corrupted\n",FileName);
		printf("      %i : %s\n\n",gSystem->GetErrno(),gSystem->GetError());
		gSystem->ResetErrno();
		File->Close();
		delete File;
		return false;
	}
	
	File->Close();
	delete File;
	return true;
}

class TOption
{
private :
	
	bool Usage();
public :
	TOption()
	{
		nentries=-1;
		FirstEntry=0;
		strcpy(OPTIONS," ");
		debug=false;
		stdoutput=false;
		Check=true;
		testmode=false;
		NFile=100;
		useIrods = false;
		openIrods= false;
		copyIrods=false;
		useHPSS  = false;
		prefix=NULL;
	}
	
	~TOption(){};
	
	vector <char*> InputFile;
	char Script[100];
	char OPTIONS[1074];
	long int nentries;
	long int FirstEntry;
	double STARTAT, ENDAT;
	bool merge;
	bool debug;
	bool stdoutput;
	bool draw;
	bool Check;
	bool useIrods;
	bool openIrods;
	bool copyIrods;
	bool useHPSS;
	char queue[100];
	char vmem[5];
	char fsize[5];
	char DataTag[1074];
	char *OutputROOT;
	bool testmode;
	unsigned int NFile;
	char *prefix;
	
	bool HandleInputPar(int, char **);
	bool Merge();
};

//////////////////////////////////////////////////////////////////////////////////////////////////
//! \brief Merge Multiple root file together by adding all histograms to each other.
bool TOption::Merge()
{
	cout<<"\033[22;31m---->> Merging\033[0m"<<endl;
	
	vector <TString*> ObjName;
 	vector <TString*> ObjClass;
	vector <TString*> ObjRef;
	vector <double>   NObject;
	
	TObject* obj;
	bool ClassMatch=false;
	
	double INEntries, OUTEntries;
	
	TFile *f;
	
	TString TheFileName;
	if(strstr(OPTIONS,".root") != NULL)
	{
		TheFileName = OPTIONS;
		TheFileName.Remove(TheFileName.Index(".root")+5,TheFileName.Length()-(TheFileName.Index(".root"))+5);
		//printf("      \033[22;33m%s\033[0m\n",TheFileName.Data());
		int kTest=0;
		while(TheFileName.Contains(" "))
		{
			TheFileName.Remove(0,TheFileName.Index(" ")+1);
			kTest++;
			if(kTest > 100)
			{
				printf("\033[43;31m!!! ERROR IN EXTRACTING INPUT FILE NAME\033[0m\n      Set Default File name.\n");
				TheFileName="output.root";
				break;
			}
		}
		//printf("      \033[22;33m%s\033[0m\n",TheFileName.Data());
	}
	else TheFileName="output.root";
	if(!TheFileName.Contains(".root")) TheFileName="output.root";
	TFile *outfh = new TFile(TheFileName.Data(),"RECREATE");
	printf("   - Output file \033[22;34m%s\033[0m created.\n",outfh->GetName());
	
	TH1 *TH1IN, *TH1OUT;
	TH2 *TH2IN, *TH2OUT;
	TH3 *TH3IN, *TH3OUT;
	
	vector <char*> ListOfFile;
	DIR *dir;
	struct dirent *ent;
	for(unsigned int dd=0; dd < InputFile.size(); dd++)
	{
		if(strstr(InputFile[dd],".root") == NULL)
		{
			dir = opendir(InputFile[dd]);
			if (dir != NULL)
			{
				while ((ent = readdir (dir)) != NULL)
				{
					if(strstr(ent->d_name,".root") != NULL)
					{
						ListOfFile.push_back(new char[strlen(InputFile[dd])+strlen(ent->d_name)]);
						strcpy(ListOfFile.at(ListOfFile.size()-1),InputFile[dd]);
						strcat(ListOfFile.at(ListOfFile.size()-1),"/");
						strcat(ListOfFile.at(ListOfFile.size()-1),ent->d_name);
						printf("   - \033[25;31mAdding file to list of file to be merged \033[0m: %s\n",ListOfFile.at(ListOfFile.size()-1));
					}
				}
				closedir (dir);
			}
			else
			{
				printf("\033[43;31m!!!! ERROR !!!!\033[0m\n   Directory %s does not exist or can not be opened.\n\n",InputFile[0]);
				continue;
			}
		}
		else
		{
			ListOfFile.push_back(new char[strlen(InputFile.at(dd))]);
			strcpy(ListOfFile.at(ListOfFile.size()-1),InputFile[dd]);
			printf("   - \033[25;31mAdding file to list of file to be merged \033[0m: %s\n",ListOfFile.at(ListOfFile.size()-1));
		}
		
	}
	if(ListOfFile.size() <= 0) return 0;
	
	for (unsigned int idx=0; idx < ListOfFile.size(); idx++)
	{
		// OPEN NEW INPUT FILE AND OUTPUT FILE FOR UPDATE
		
		f = new TFile((const char*) ListOfFile.at(idx),"READ");
		if(f->IsZombie())
		{
			f->Close();
			delete f;
			printf("\033[22;31m!!! File %s made zombie and ignored\n\033[0m",f->GetName());
			continue;
		}
		if(debug) cout<<endl<<endl;
		printf("   - Merging file \033[22;34m%s\033[0m\n",f->GetName());
		
		// 2- Get the names, classes and number of objects in the File to be merged
		TIter next(f->GetListOfKeys());
		TKey *Key(0);
		
		while(Key=(TKey*)next())
		{
			obj=Key->ReadObj();
			ObjName.push_back (new TString(obj->GetName()));
			ObjClass.push_back(new TString(Key->GetClassName()));
		}
		
		// ADD EACH OBJECT OF SAME NAME TOGETHER
		for(unsigned int j=0;j<ObjName.size();j++)
		{
			
			unsigned int N=0;
			
			if(ObjClass.at(j)->Contains("TH1"))
			{
				if(idx == 0)
				{
					printf("          <\033[22;34m%s\033[22;30m> \033[22;31m%s\033[0m\n",ObjClass.at(j)->Data(),ObjName.at(j)->Data());
					cout<<"            ---> NEW OBJECT ADD TO LIST"<<endl;
					ObjRef.push_back(new TString(ObjName.at(j)->Data()));
					NObject.push_back(1);
					TH1IN = (TH1*) f->Get(ObjName.at(j)->Data());
					TH1OUT= (TH1*) TH1IN->Clone();
					outfh->cd();
					TH1OUT->Write(TH1OUT->GetName(),TH1OUT->kOverwrite);
					TH1IN->Reset();
					TH1OUT->Reset();
					continue;
				}
				else
				{
					N=0;
					ClassMatch=false;
					while(!ClassMatch && N < ObjRef.size()){ if(!strcmp(ObjRef.at(N)->Data(),ObjName.at(j)->Data())) ClassMatch=true; else N++;}
					if(!ClassMatch)
					{
						printf("          <\033[22;34m%s\033[22;30m> \033[22;31m%s\033[0m\n",ObjClass.at(j)->Data(),ObjName.at(j)->Data());
						cout<<"            ---> NEW OBJECT ADD TO LIST"<<endl;
						ObjRef.push_back(new TString(ObjName.at(j)->Data()));
						NObject.push_back(1);
						TH1IN = (TH1*) f->Get(ObjName.at(j)->Data());
						TH1OUT= (TH1*) TH1IN->Clone();
						outfh->cd();
						TH1OUT->Write(TH1OUT->GetName(),TH1OUT->kOverwrite);
						TH1IN->Reset();
						TH1OUT->Reset();
						continue;
					}
					//else
					//{
					//	if(debug) printf("   ClassMatch % (%s).%s and (%s).%s\n",f->GetName(),ObjRef.at(N)->Data(),outfh->GetName(),ObjName.at(N)->Data());
					//}
				}
				
				
				TH1IN = (TH1*) f->Get(ObjName.at(j)->Data());
				INEntries=TH1IN->GetEntries();
				//if(INEntries <= 0){continue;}
				
				TH1OUT= (TH1*) outfh->Get(ObjName.at(j)->Data());
				if(TH1OUT==NULL)
				{
					printf("!!!! ERROR !!!! Output histogram \033[22;34m%s\033[22;0m is Zombie\n",ObjName.at(j)->Data());
					TH1IN->Reset();
					continue;
				}
				OUTEntries=TH1OUT->GetEntries();
				
				if(TH1OUT->GetNbinsX() != TH1IN->GetNbinsX())
				{
					printf("*** WARNING *** Histograms \033[22;34m%s\033[22;0m and \033[22;34m%s\033[22;0m does not have same X axis.\n",TH1IN->GetName(), TH1OUT->GetName());
					continue;
				}
				
				if(debug) printf("   MERGE HISTO \033[22;34m%s\033[22;0m with \033[22;34m%s\033[22;0m.",TH1OUT->GetName(),TH1IN->GetName());
				
			    TH1OUT->Add(TH1IN,1);
				TH1OUT->SetEntries(INEntries+OUTEntries);
				outfh->cd();
				TH1OUT->Write(TH1OUT->GetName(),TH1OUT->kOverwrite);
				
				if(debug) printf("   \033[22;34m%s\033[22;0m ne entries : %.0f (%.0f).\n",TH1OUT->GetName(),TH1OUT->GetEntries(),TH1IN->GetEntries());
				
				NObject.at(N)++;
				
				//TH1OUT->Reset();
				TH1IN->Reset();
			}
			else if(ObjClass.at(j)->Contains("TH2"))
			{
				
				if(idx == 0)
				{
					printf("          <\033[22;34m%s\033[22;30m> \033[22;31m%s\033[0m\n",ObjClass.at(j)->Data(),ObjName.at(j)->Data());
					cout<<"            ---> NEW OBJECT ADD TO LIST"<<endl;
					ObjRef.push_back(new TString(ObjName.at(j)->Data()));
					NObject.push_back(1);
					TH2IN = (TH2*) f->Get(ObjName.at(j)->Data());
					TH2OUT= (TH2*) TH2IN->Clone();
					outfh->cd();
					TH2OUT->Write(TH2OUT->GetName(),TH2OUT->kOverwrite);
					continue;
				}
				else
				{
					N=0;
					ClassMatch=false;
					while(!ClassMatch && N < ObjRef.size()){if(!strcmp(ObjRef.at(N)->Data(),ObjName.at(j)->Data())) ClassMatch=true; else N++;}
					if(!ClassMatch)
					{
						printf("          <\033[22;34m%s\033[22;30m> \033[22;31m%s\033[0m\n",ObjClass.at(j)->Data(),ObjName.at(j)->Data());
						cout<<"            ---> NEW OBJECT ADD TO LIST"<<endl;
						ObjRef.push_back(new TString(ObjName.at(j)->Data()));
						NObject.push_back(1);
						TH2IN = (TH2*) f->Get(ObjName.at(j)->Data());
						TH2OUT= (TH2*) TH2IN->Clone();
						outfh->cd();
						TH2OUT->Write(TH2OUT->GetName(),TH2OUT->kOverwrite);
						continue;
					}
					//else
					//{
					//	if(debug) printf("   ClassMatch % (%s).%s and (%s).%s\n",f->GetName(),ObjRef.at(N)->Data(),outfh->GetName(),ObjName.at(N)->Data());
					//}
				}
				
				TH2IN = (TH2*) f->Get(ObjName.at(j)->Data());
				INEntries=TH2IN->GetEntries();
				//if(INEntries <= 0){continue;}
				
				TH2OUT= (TH2*) outfh->Get(ObjName.at(j)->Data());
				if(TH2OUT==NULL)
				{
					printf("!!!! ERROR !!!! Output histogram \033[22;34m%s\033[22;0m is Zombie\n",ObjName.at(j)->Data());
					TH2IN->Reset();
					continue;
				}
				OUTEntries=TH2OUT->GetEntries();
				
				if(TH2OUT->GetNbinsX() != TH2IN->GetNbinsX())
				{
					printf("*** WARNING *** Histograms \033[22;34m%s\033[22;0m and \033[22;34m%s\033[22;0m does not have same X axis.\n",TH2IN->GetName(), TH2OUT->GetName());
					continue;
				}
				if(TH2OUT->GetNbinsY() != TH2IN->GetNbinsY())
				{
					printf("*** WARNING *** Histograms \033[22;34m%s\033[22;0m and \033[22;34m%s\033[22;0m does not have same Y axis.\n",TH2IN->GetName(), TH2OUT->GetName());
					continue;
				}
				
				if(debug) printf("   MERGE HISTO \033[22;34m%s\033[22;0m with \033[22;34m%s\033[22;0m.",TH2OUT->GetName(),TH2IN->GetName());
				
			    TH2OUT->Add(TH2IN);
				TH2OUT->SetEntries(INEntries+OUTEntries);
				outfh->cd();
				TH2OUT->Write(TH2OUT->GetName(),TH2OUT->kOverwrite);
				
				if(debug) printf("   \033[22;34m%s\033[22;0m ne entries : %.0f (%.0f).\n",TH2OUT->GetName(),TH2OUT->GetEntries(),TH2IN->GetEntries());
				
				NObject.at(N)++;
				
				//TH2OUT->Reset();
				TH2IN->Reset();
			}
			else if(ObjClass.at(j)->Contains("TH3"))
			{
				if(idx == 0)
				{
					printf("          <\033[22;34m%s\033[22;30m> \033[22;31m%s\033[0m\n",ObjClass.at(j)->Data(),ObjName.at(j)->Data());
					cout<<"            ---> NEW OBJECT ADD TO LIST"<<endl;
					ObjRef.push_back(new TString(ObjName.at(j)->Data()));
					NObject.push_back(1);
					TH3IN = (TH3*) f->Get(ObjName.at(j)->Data());
					TH3OUT= (TH3*) TH3IN->Clone();
					outfh->cd();
					TH3OUT->Write(TH3OUT->GetName(),TH3OUT->kOverwrite);
					continue;
				}
				else
				{
					N=0;
					ClassMatch=false;
					while(!ClassMatch && N < ObjRef.size()){if(!strcmp(ObjRef.at(N)->Data(),ObjName.at(j)->Data())) ClassMatch=true; else N++;}
					if(!ClassMatch)
					{
						printf("          <\033[22;34m%s\033[22;30m> \033[22;31m%s\033[0m\n",ObjClass.at(j)->Data(),ObjName.at(j)->Data());
						cout<<"            ---> NEW OBJECT ADD TO LIST"<<endl;
						ObjRef.push_back(new TString(ObjName.at(j)->Data()));
						NObject.push_back(1);
						TH3IN = (TH3*) f->Get(ObjName.at(j)->Data());
						TH3OUT= (TH3*) TH3IN->Clone();
						outfh->cd();
						TH3OUT->Write(TH3OUT->GetName(),TH3OUT->kOverwrite);
						continue;
					}
					//else
					//{
					//	if(debug) printf("   ClassMatch % (%s).%s and (%s).%s\n",f->GetName(),ObjRef.at(N)->Data(),outfh->GetName(),ObjName.at(N)->Data());
					//}
				}
				
				TH3IN = (TH3*) f->Get(ObjName.at(j)->Data());
				INEntries=TH3IN->GetEntries();
				//if(INEntries <= 0){continue;}
				
				TH3OUT= (TH3*) outfh->Get(ObjName.at(j)->Data());
				if(TH3OUT==NULL)
				{
					printf("!!!! ERROR !!!! Output histogram \033[22;34m%s\033[22;0m is Zombie\n",ObjName.at(j)->Data());
					TH3IN->Reset();
					continue;
				}
				OUTEntries=TH3OUT->GetEntries();
				
				if(TH3OUT->GetNbinsX() != TH3IN->GetNbinsX())
				{
					printf("*** WARNING *** Histograms \033[22;34m%s\033[22;0m and \033[22;34m%s\033[22;0m does not have same X axis.\n",TH3IN->GetName(), TH3OUT->GetName());
					continue;
				}
				if(TH3OUT->GetNbinsY() != TH3IN->GetNbinsY())
				{
					printf("*** WARNING *** Histograms \033[22;34m%s\033[22;0m and \033[22;34m%s\033[22;0m does not have same Y axis.\n",TH3IN->GetName(), TH3OUT->GetName());
				continue;
				}
				if(TH3OUT->GetNbinsZ() != TH3IN->GetNbinsZ())
				{
					printf("*** WARNING *** Histograms \033[22;34m%s\033[22;0m and \033[22;34m%s\033[22;0m does not have same Z axis.\n",TH3IN->GetName(), TH3OUT->GetName());
					continue;
				}
				
			    TH3OUT->Add(TH3IN);
				TH3OUT->SetEntries(INEntries+OUTEntries);
				outfh->cd();
				TH3OUT->Write(TH3OUT->GetName(),TH3OUT->kOverwrite);
				
				NObject.at(N)++;
				
				//TH3OUT->Reset();
				TH3IN->Reset();
			}
			else{continue;}
			
		}
		if(!ObjName.empty())
		{
			for(unsigned int i=0; i< ObjName.size(); i++) 
				delete ObjName[i]; 
			ObjName.clear();
		}
		if(!ObjClass.empty())
		{
			for(unsigned int i=0; i< ObjClass.size(); i++) 
				delete ObjClass[i]; 
			ObjClass.clear();
		}
		
		f->Close();
		delete f;
	}
	
	cout<<endl;
	for(unsigned int N=0; N<ObjRef.size(); N++)
	{
		cout<<"     \033[22;32m"<<NObject.at(N)<<"\033[0m object \033[22;31m"<<ObjRef.at(N)->Data()<<"\033[0m merged succesfully."<<endl;
		delete ObjRef[N];
	}
	ObjRef.clear();
	NObject.clear();
	
	outfh->Close();
	delete outfh;
	
	for (unsigned int idx=0; idx < ListOfFile.size(); idx++)
	{
		delete [] ListOfFile.at(idx);
	}
	
	
	cout<<"\033[22;31m<<---- Merging\033[0m"<<endl;
	return true;
}


//////////////////////////////////////////////////////////////////////////////////////////////////
//! \brief Help
bool TOption::Usage()
{
	printf("\033[22;31m*****************************************************************************************\n");
#ifdef __JobResub__
	printf("\033[22;30mJob submission : \033[22;34mHelp\n");
	printf("\033[22;31m*****************************************************************************************\n");
	printf("\033[0mCreate a scripte to submit AMS analyses job to the Grid Engine.\n");
	
	printf("\n\033[22;31mSYNOPSY\n");
	printf("\033[0mJobResub.exe  [\033[22;34m-NFile \033[0mN] [\033[22;34m-date \033[0m StartAt EndAt] [\033[22;34m-irod/-hpss\033[0m] [\033[22;34m-icopy\033[0m]\n");
	printf("              [\033[22;34m-p \033[0mprefix] [\033[22;34m-q \033[0mqueue] [\033[22;34m-vmem \033[0mvmem] [\033[22;34mAMSAnalyser's options \033[0m]\n");
	printf("              \033[22;31mSCRIPT \033[22;34m Input dir\033[0m  \033[22;34m Output dir\033[0m\n");
#else
	printf("\033[22;30mAMS Analyse : \033[22;34mHelp\n");
	printf("\033[22;31m*****************************************************************************************\n");
	printf("\033[0mApply a selection script on a list of AMS rootuple files to slect specific event and plot\n histograms.\n");
	
	printf("\n\033[22;31mSYNOPSY\n");
	printf("\033[0mAMSAnalyser.exe [\033[22;34m-outputFile \033[0mFileName.root] [\033[22;34m-NEntries \033[0mN] \n");
	printf("                [\033[22;34m-min \033[0mN0] [\033[22;34m+CloneTree\033[0m] [\033[22;34m-RangeDate \033[0m StartAt EndAt] \n");
	printf("                [\033[22;34m-merge \033[0m] \033[22;31m SCRIPT [\033[22;34m Input Files\033[0m]  \n");
#endif
	
	printf("\n\033[22;31mREQUIRED PARAMETERS\n");
	printf("\033[22;34mSCRIPT \033[0m\n");
	printf("     The name of the Selection script to be loaded.\n");


#ifdef __JobResub__
	printf("\033[22;34mInput dir \033[0m\n");
	printf("     The directory which contains the file to be anlysed.\n\n");
	printf("\033[22;34mOutput dir \033[0m\n");
	printf("     The directory which contains the output file which have already been processed.\n");
#endif	
	
		
	printf("\n\033[22;31mOPTIONAL PARAMETERS\n");
#ifdef __JobResub__
	printf("\n\033[22;34m-NFile \033[0mFileName\n");
	printf("     Number of file to be processed per GRID ENGINE job.\n");
	printf("\n\033[22;34m-irod/hpss \033[0m\n");
	printf("     Specify that the output dir is an IRODS or HPSS virtual disk.\n");
	printf("     Disabling this option will search the output dir on the physical disk map.\n");
	printf("     When using iRods, the output file will automatically be created into the output dir.\n In that case, be sure the output dir exists.\n");
	printf("\n\033[22;34m-icopy \033[0m\n");
	printf("     The inputfile is copyed locally and opened from physical disk instead of using the xrootd protocol.\n");
	printf("\n\033[22;34m-p \033[0mprefix\n");
	printf("     Prefix of the output file.\n");
	printf("\n\033[22;34m-q \033[0mqueue\n");
	printf("     Nam of the GRID ENGINE queue to which the job has to be submited.\n");
	printf("\n\033[22;34m-vmem \033[0mvmem\n");
	printf("     Maximal memory size requested for the job\n");	
#else	
	printf("\n\033[22;34m-outputFile \033[0mFileName\n");
	printf("     Path and name of the output root file which will contains all User's histograms.\n");
	printf("\n\033[22;34m+CloneTree \033[0m\n");
	printf("     Option to clone selected event into the output rootfile.\n");
	printf("\n\033[22;34m-NEntries \033[0m N\n");
	printf("     Total number of event to analyse. By default, all event are analysed.\n");
	printf("\n\033[22;34m-min \033[0m N0\n");
	printf("     The first event to be analysed. By default it start at the first event.\n");
	printf("\n\033[22;34m Input Files \033[0m\n");
	printf("     Path and name of the file to be analysed. By default, it load all rootuple in the\n     current directory.\n");
	printf("\n\033[22;34m-Merge \033[0mor \033[22;34m-merge\033[0m\n");
	printf("     Run program in merging mode. In this case, SCRIPT is optional and, if given,\n");
	printf("     it will be ignored. All histograms stored in the files listed as Input files\n");
	printf("      will be merged together into the specified output file.\n");
#endif


	printf("\n\033[22;34m-date \033[0mStartAt EndAt\n");
#ifdef __JobResub__
	printf("     Range limit, in UNIX time stamp, of the DAQ date that need to be processed.\n");
#else
	printf("     Range limit, in ddmmyy.hhmmss, of the DAQ date that need to be processed.\n");
#endif
	printf("\n\033[22;34m+debug \033[0mor \033[22;34m+Debug\033[0m\n");
	printf("     Set debug mode.\n");
	printf("\n\033[22;34m-help \033[0mor \033[22;34m--h\033[0m\n");
	printf("     Show this message.\n");
	printf("\n\033[22;32m*****************************************************************************************\033[0m\n\n\n");
	
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//! \brief Read input parameters
bool TOption::HandleInputPar(int argc, char **argv)
{
	
	nentries=-1;
	FirstEntry=0;
	STARTAT=0;
	ENDAT=0;
	merge=false;
	debug=false;
	draw =false;
	Check=true;
	strcpy(DataTag,"");
	strcpy(OPTIONS," ");
	OutputROOT=NULL;
	strcpy(queue,"huge");
	strcpy(vmem,"6G");
	strcpy(fsize,"50G");
	
	if(argc <= 1) {printf("\033[43;31m!!!! ERROR !!!!\033[0m\n   Not enought number of argumen\n\n"); return 0;}
	
	for (int n = 0; n < argc; n++)
	{
		if(!strcmp(argv[n], "--h") || !strcmp(argv[n], "-help") || !strcmp(argv[n], "-h") || !strcmp(argv[n], "--help")) return Usage();
	}
	
	printf("\033[22;31m---->> READ INPUT PARAMETERS\033[0m\n");
		
	// -----------------------
	// Read input parameters
	// -----------------------  
	
	int ARG;
	for (int i = 1; i < argc; i++)
	{		
		ARG=i;
		if(strstr(argv[i], ".C") || strstr(argv[i], ".C+") || strstr(argv[i], ".C++"))
		{
			break;
		}
        //------------------------------------//
		else if (!strcmp(argv[i], "-q"))
		{
			if (++i >= argc)
				i--;
			strcpy(queue,argv[i]);
			if(!strcmp(queue,"huge"))
				strcpy(fsize,"50G");
			else if(!strcmp(queue,"long"))
				strcpy(fsize,"10G");
			else if(!strcmp(queue,"medium"))
				strcpy(fsize,"10G");
			else if(!strcmp(queue,"short"))
				strcpy(fsize,"4G");
			else if(!strcmp(queue,"mc_long"))
			{
				strcpy(fsize,"30G");
				strcat(queue," -pe multicores 20");
			}
			continue;
		}
        //------------------------------------//
		else if (!strcmp(argv[i], "-vmem"))
		{
			if (++i >= argc)
				i--;
			strcpy(vmem,argv[i]);
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-fsize"))
		{
			if (++i >= argc)
				i--;
			strcpy(fsize,argv[i]);
			continue;
		}
        //------------------------------------//
		else if (!strcmp(argv[i], "-NFile") || !strcmp(argv[i], "-Nfile") || !strcmp(argv[i], "-nfile") || !strcmp(argv[i], "-nFile"))
		{
			if (++i >= argc)
				i--;
			NFile = atoi(argv[i]);
			printf("   - \033[22;34mNumber of file per Job \033[0m:%i\n",NFile);
			continue;
		}
        //------------------------------------//
		else if (!strcmp(argv[i], "-serie") || !strcmp(argv[i], "-Serie"))
		{
			continue;
		}
        //------------------------------------//
		else if (!strcmp(argv[i], "-irods"))
		{
        	    useIrods=true;
        	    continue;
       	 }
		//------------------------------------//
		else if (!strcmp(argv[i], "-icopy"))
		{
			copyIrods=true;
			continue;
		}
		else if (!strcmp(argv[i], "-iopen"))
		{
			useIrods=true;
			openIrods=true;
			continue;
		}
        //------------------------------------//
		else if (!strcmp(argv[i], "-rfcdir"))
        {
            		useHPSS=true;
           		 continue;
       	 }
		//------------------------------------//
		else if (!strcmp(argv[i], "-tag") || !strcmp(argv[i], "-Tag") || !strcmp(argv[i], "-TAG"))
		{
			if (++i >= argc) {i--; continue;}
			strcpy(DataTag,argv[i]);
			
			printf("   - \033[22;34mData Tage \033[0m:%s\n",DataTag);
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-p"))
		{
			if (++i >= argc) {i--; continue;}
			prefix=new char[strlen(argv[i])+5];
			strcpy(prefix,argv[i]);
			
			printf("   - \033[22;34mPrefix \033[0m:%s\n",prefix);
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-test") || !strcmp(argv[i], "-TEST") || !strcmp(argv[i], "-Test"))
		{
			testmode=true;
			printf("   - \033[22;34mRun Test \033[0m\n");
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-outputFile") || !strcmp(argv[i], "-outputfile") || !strcmp(argv[i], "-OutputFile"))
		{
			if (++i >= argc) {i--; continue;}
			if(strstr(argv[i],".root") == NULL) {i--; continue;}
			if(!strcmp(OPTIONS," "))
				strcpy(OPTIONS,argv[i]);
			else
				strcat(OPTIONS,argv[i]);
			
			OutputROOT=new char [strlen(argv[i])];
			strcpy(OutputROOT,argv[i]);
			strcat(OPTIONS," ");
			printf("   - \033[22;34mOutput File name \033[0m:%s\n",OutputROOT);
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "+Draw") || !strcmp(argv[i], "+draw"))
		{
			if(!strcmp(OPTIONS," "))
				strcpy(OPTIONS,"Draw ");
			else
				strcat(OPTIONS,"Draw ");
			draw=true;
			printf("   - \033[22;34mSet Draw \033[0m\n");
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-check") || !strcmp(argv[i], "-CHECK"))
		{
			Check=false;
			printf("   - \033[22;34mDont Check Validity of Root File \033[0m\n");
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-Merge") || !strcmp(argv[i], "-merge"))
		{
			merge=true;
			printf("   - \033[22;34mSet Merge \033[0m\n");
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-nentries") || !strcmp(argv[i], "-Nentries") || !strcmp(argv[i], "-NEntries") || !strcmp(argv[i], "-nEntries"))
		{
			if (++i >= argc) {i--; continue;}
			nentries = atol(argv[i]);
			printf("   - \033[22;34mAnalyse \033[0m %li\033[22;34m events \033[0m\n",nentries);
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-min") ||
				 !strcmp(argv[i], "-Min") || 
				 !strcmp(argv[i], "-MIN"))
		{
			if (++i >= argc) {i--; continue;}
			FirstEntry = atol(argv[i]);
			printf("   - \033[22;34mStart analyse @ event # \033[0m %li\n",FirstEntry);
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "-RangeDate") ||
				 !strcmp(argv[i], "-rangeDate") || 
				 !strcmp(argv[i], "-rangedate") || 
				 !strcmp(argv[i], "-RangeDate") ||
				 !strcmp(argv[i], "-Date")      ||
				 !strcmp(argv[i], "-date")      ||
				 !strcmp(argv[i], "-DATE")      )
		{
			if (++i >= argc) {i--; continue;}
			STARTAT = atof(argv[i]);
			if (++i >= argc) {i--; continue;}
			ENDAT   = atof(argv[i]);
			printf("   - \033[22;34mAnalyses data collected between \033[0m %.2f \033[22;34m and \033[0m %.2f\n",STARTAT,ENDAT);
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "+Debug") || !strcmp(argv[i], "+debug") || !strcmp(argv[i], "-debug") || !strcmp(argv[i], "-Debug"))
		{
			if(!strcmp(OPTIONS," "))
				strcpy(OPTIONS,"debug ");
			else
				strcat(OPTIONS,"debug ");
			printf("   - \033[22;34mSet debug mode \033[0m\n");
			debug=true;
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "+Verbose") || !strcmp(argv[i], "+VERBOSE") || !strcmp(argv[i], "+verbose") || !strcmp(argv[i], "-VERBOSE") || !strcmp(argv[i], "-Verbose") || !strcmp(argv[i], "-verbose"))
		{
			printf("   - \033[22;34mSet verbose mode \033[0m\n");
			stdoutput=true;
			continue;
		}
		//------------------------------------//
		else if (!strcmp(argv[i], "+CloneTree") || !strcmp(argv[i], "+Clonetree") || !strcmp(argv[i], "+clonetree") || !strcmp(argv[i], "+cloneTree"))
		{
			if(!strcmp(OPTIONS," "))
				strcpy(OPTIONS,"+CloneTree ");
			else
				strcat(OPTIONS,"CloneTree ");
			printf("   - \033[22;34mAllow clonning of selected event \033[0m\n");
			continue;
		}
		else
		{
			if(!strcmp(OPTIONS," "))
				strcpy(OPTIONS,argv[i]);
			else
				strcat(OPTIONS,argv[i]);
			strcat(OPTIONS," ");
			printf("   - \033[22;34mAdd option %s\033[0m\n",argv[i]);
			continue;
		}
			
	}
	
	if(!merge)
	{
		if(ARG >= argc) {printf("\033[43;31m!!!! ERROR !!!!\033[0m\n   Not enought number of argumen\n\n"); return 0;}
		while(strstr(argv[ARG], ".C") == NULL) {ARG++; if(ARG >= argc) break;}
	
		if(ARG >= argc) {printf("\033[43;31m!!!! ERROR !!!!\033[0m\n   Not enought number of argumen\n\n"); return 0;}
		if(strstr(argv[ARG], ".C") != NULL)
		{
			strcpy(Script,argv[ARG]);
			if(strstr(Script, ".C+") == NULL) strcat(Script,"+");
			printf("   - \033[22;31mScript Name \033[0m:%s\n",Script);
		}
		else
		{
			printf("\033[43;31m!!!! ERROR !!!!\033[0m\n   Script Missing\n\n"); return 0;
		}
	
		ARG++;
	
		while(ARG < argc)
		{
			InputFile.push_back(new char[500]);
			strcpy(InputFile.at(InputFile.size()-1),argv[ARG]);
						
			if(strstr(argv[ARG],"pass2") == NULL &&
			   strstr(argv[ARG],"pass3") == NULL &&
			   strstr(argv[ARG],"pass4") == NULL &&
			   strstr(argv[ARG],"pass5") == NULL &&
			   strstr(argv[ARG],"pass6") == NULL &&
			   strstr(argv[ARG],"pass7") == NULL &&
			   strstr(argv[ARG],"pass8") == NULL &&
			   strstr(argv[ARG],"std"  ) == NULL  )
			{
				if(strstr(OPTIONS,"usetimediff") == NULL)
					strcat(OPTIONS," usetimediff ");
			}

			ARG++;
		}
	}
	else
	{		
		while(ARG < argc)
		{
			InputFile.push_back(new char[500]);
			strcpy(InputFile.at(InputFile.size()-1),argv[ARG]);


			if(strstr(argv[ARG],".root")==NULL) printf("   - \033[25;31mDirectory of files to be merged \033[0m: %s\n",InputFile.at(InputFile.size()-1));
			else                                printf("   - \033[25;31mFiles to be merged \033[0m             : %s\n",InputFile.at(InputFile.size()-1));
			ARG++;
		}
	}
	
	
	printf("\033[22;31m----<< DONE\033[0m\n");
	return 1;
}
