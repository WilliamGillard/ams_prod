//
//  CheckFile.cxx
//  
//
//  Created by gillard on 26/07/13.
//
//

#include <TFile.h>
#include <TTree.h>
#include <TList.h>
#include <TSystem.h>
#include <TObject.h>

int main(int argc, char **argv)
{
	
	TFile *File2Bcheck;
	bool completed=true;
	bool checkTree=true;
	bool updateTree=false;
	TTree *RecEvent;
	TTree *UpEvent = NULL;
	int NFailed = 0;
	
	for(int _f=0; _f<argc; _f++)
	{
		File2Bcheck	=	NULL;
		RecEvent	=	NULL;
		UpEvent		=	NULL;
		
		if(strstr(argv[_f],"-tree"))
			checkTree=false;
		
		if(strstr(argv[_f],"+uptree"))
			updateTree=true;

		if(!strstr(argv[_f],".root"))
			continue;
				
		File2Bcheck=TFile::Open(argv[_f],"READ");
		
		if(File2Bcheck == NULL)
		{
			printf("\033[43;31m*** %s : FILE IS MISSING ***\033[0m\n",argv[_f]);
			NFailed++;
			continue;
		}
		
		if(File2Bcheck->IsZombie())
		{
			printf("\033[43;31m*** %s : FILE MADE ZOMBIE ***\033[0m\n",argv[_f]);
			File2Bcheck->Close();
			gSystem->Exec(Form("rm -f %s",argv[_f]));
			NFailed++;
			continue;
		}
		
		if(updateTree)
		{
			UpEvent  = (TTree*) File2Bcheck->Get("EventUpdate");
			
			if(UpEvent == NULL)
			{
				printf("\033[43;31m*** %s : UPDATED TREE IS MISSING ***\033[0m\n",argv[_f]);
				File2Bcheck->Close();
				gSystem->Exec(Form("rm -f %s",argv[_f]));
				NFailed++;
				continue;
			}
			
			if(UpEvent->IsZombie())
			{
				printf("\033[43;31m*** %s : UPDATED TREE IS ZOMBIE ***\033[0m\n",argv[_f]);
				File2Bcheck->Close();
				gSystem->Exec(Form("rm -f %s",argv[_f]));
				NFailed++;
				continue;
			}
			
			if(UpEvent->GetEntries() <= 0)
			{
				printf("\033[43;31m*** %s : UPDATED TREE IS INCOMPLET ***\033[0m\n",argv[_f]);
				File2Bcheck->Close();
				gSystem->Exec(Form("rm -f %s",argv[_f]));
				NFailed++;
				continue;
			}
		}

		
		if(!checkTree)
			continue;
		
		RecEvent = (TTree*) File2Bcheck->Get("RecEvent");
		
		if(RecEvent == NULL)
		{
			printf("\033[43;31m*** %s : TREE IS MISSING ***\033[0m\n",argv[_f]);
			File2Bcheck->Close();
			gSystem->Exec(Form("rm -f %s",argv[_f]));
			NFailed++;
			continue;
		}
		
		if(RecEvent->IsZombie())
		{
			printf("\033[43;31m*** %s : TREE IS MADE ZOMBIE ***\033[0m\n",argv[_f]);
			File2Bcheck->Close();
			gSystem->Exec(Form("rm -f %s",argv[_f]));
			NFailed++;
			continue;
		}
		
		if(updateTree)
		{
			if(UpEvent->GetEntries() < RecEvent->GetEntries())
			{
				printf("\033[43;31m*** %s : UPDATED TREE IS INCOMPLET ***\033[0m\n",argv[_f]);
				File2Bcheck->Close();
				gSystem->Exec(Form("rm -f %s",argv[_f]));
				NFailed++;
				continue;
			}
		}

		Long64_t nbytes = 0, nb = 0;
		for (Long64_t jentry=0; jentry < RecEvent->GetEntries() ; jentry++)
		  {
		    nb = RecEvent->GetEntry(jentry);   nbytes += nb;
		    if(nb<0)
		      {
			printf("\033[43;31m*** %s : CORRUPTED BRANCH ***\033[0m\n",argv[_f]);
			  File2Bcheck->Close();
			  gSystem->Exec(Form("rm -f %s",argv[_f]));
			  NFailed++;
			  break;
		      }
		  }
		if(nb < 0)
		  continue;
		    
		
		completed=false;
		TList *userInfo=RecEvent->GetUserInfo();
		TIter it((TList*) userInfo);
		TObject *obj;
		
		while ((obj = it()))
		{
			if(strstr(((TObjString*) obj)->GetString().Data(),"CPU") != NULL)
				completed=true;
		}
		
		if(!completed)
		{
			printf("\033[43;31m*** %s : TREE IS INCOMPLET ***\033[0m\n",argv[_f]);
			File2Bcheck->Close();
			gSystem->Exec(Form("rm -f %s",argv[_f]));
			NFailed++;
			continue;
		}
		
		printf("%s [\033[32mVALID\033[0m]\n",argv[_f]);
	}
		
	printf("\n");
	return NFailed;
		
		
}
	
