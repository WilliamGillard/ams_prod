//
//  JobResub.cxx
//  
//
//  Created by Gillard on 11/03/13.
//
//

#ifndef __JobResub__
#define __JobResub__
#endif

#include "AMSAnalyser.h"

#include <stdio.h>
#include <vector>
#include <map>
using namespace std;

#include <LxAMSdata.h>
#include <LxAMSdataManager.h>
#include <FileListing.h>

#include <TString.h>
#include <TFile.h>
#include <TSystem.h>

//typedef std::map <double,int> fList;
typedef std::map <vector<int>,int> fList;

int main(int argc, char **argv)
{
    int Nmiss=0, Ncorr=0, Nfile=0;
    int nsplit =0 ;
    bool _rfdir  = false;
    bool _irods  = false;
    bool useRunID=false;
    bool silent  =false;
    bool madeTree =true;
    bool upTree = false;
	bool force=false;

	bool mergeUp=true;
	bool spscopy=true;

    TFile *rootFile;
    TTree *RecEvent;
    vector <string> failedList;
    TOption Param=TOption();
    if(!Param.HandleInputPar(argc,argv)) return 0;
	
    if(strstr(Param.OPTIONS,"silent"))
        silent=true;

    if(strstr(Param.OPTIONS,"-update"))
    {
        upTree=true;
	}
	
	if(upTree)
		printf("\033[43;31m    ****    UPDATING TREE FILE    ****    \033[0m\n");
	
	if(strstr(Param.OPTIONS,"-force"))
    {
        force=true;
		printf("\033[43;31m    ****    FORCE REPROCESSING    ****    \033[0m\n");
    }

	if(strstr(Param.OPTIONS,"-hadd") && upTree)
    {
        mergeUp=false;
		printf("\033[43;31m    ****    DO NOT MERGE UPDATED TREE WITH OLD TREE    ****    \033[0m\n");
    }

	if(strstr(Param.OPTIONS,"+split"))
	  {
	    nsplit=2;
	    printf("\033[43;31m    ****    DIVIDE OUTPUTSCRIPT IN %i    ****    \033[0m\n",nsplit);
	  }

	if(strstr(Param.OPTIONS,"+spscopy") && upTree)
    {
        spscopy=true;
		int pose = std::string(Param.InputFile[1]).find_last_of("/");
		printf("\033[43;31m    ****    COPY OUTPUT TO /sps/ams/lpsc/tree/B620.%.*s_update    ****    \033[0m\n",strlen(Param.InputFile[1])-pose-1, Param.InputFile[1]+pose+1);
    }

	if(strstr(Param.OPTIONS,"-merge") && upTree)
    {
        mergeUp=false;
		printf("\033[43;31m    ****    DO NOT MERGE UPDATED TREE WITH OLD TREE    ****    \033[0m\n");
    }
		
   if(strstr(Param.OPTIONS,"-tree"))
        madeTree=false;
    
    if(Param.useHPSS)
    {
        _rfdir=true;
        printf("\033[43;31m   - Check HPSS directory\033[0m\n");
    }
    
    if(Param.useIrods)
    {
        _irods=true;
        printf("\033[43;31m   - Check iRODS directory\033[0m\n");
		if(Param.openIrods)
			printf("\033[43;31m   - Will open iRODS files\033[0m\n");
    }
    
	TString CCDir=Param.InputFile[0];
	if(CCDir.Contains("ISS"))
	{
		while(CCDir.Contains("ISS"))
			CCDir.Remove(0,CCDir.First(".")+1);
		CCDir.Remove(CCDir.First("/"),CCDir.Length()-(CCDir.First("/")));
	}
	if(CCDir.Contains("TB"))
	{
		CCDir="TB";
	}
	else if(CCDir.Contains("/ams/mc/"))
		CCDir="MC";
	
	printf("\033[24;31mDATA DIR : %s\033[0m\n",CCDir.Data());
	
	if(strstr(Param.InputFile[0],".txt"))
	  strcpy(Param.DataTag,"list");
	
	LxAMSdataManager::badrunfile=gSystem->ExpandPathName(Form("/sps/ams/AMSDataDir/CCALI_BadFile/CCALI_%s.BadRunList.txt",CCDir.Data()));
	LxAMSdata *pData =  LxAMSdataManager::AccessData(Param.DataTag,Param.InputFile[0]);
	pData->SetBadRunFile(LxAMSdataManager::badrunfile);
	
	vector <string> ifile = pData->GetFiles("NOCHECK","NOCHECK",false);
	Nfile=ifile.size();
	if(ifile.size() == 0)
		return 0;
	
	TString SCRIPT=Param.Script;
	SCRIPT.ReplaceAll(".C","");
	SCRIPT.ReplaceAll(".cxx","");
	SCRIPT.ReplaceAll("+","");
	
	TString PREFIX;
	if(strlen(Param.prefix) > 1)
	{
		PREFIX = Form("%s",Param.prefix);
		PREFIX.Append("_");
	}
	else
		PREFIX="";
	
	std::string SUFFIX;
	if(upTree)
		SUFFIX = std::string("_update");
	else
		SUFFIX = std::string("");
	
	
    int Stat=0;
	   
    FILE* pipe = NULL;

    if(_rfdir)
    {
        printf("   - Get content of %s%s/ \n",Param.InputFile[1],SUFFIX.c_str());
        pipe = popen(Form("rfdir %s%s/ | grep .root",Param.InputFile[1],SUFFIX.c_str()), "r");
    }
    else if(_irods)
    {
        printf("   - Get content of %s%s/ \n",Param.InputFile[1],SUFFIX.c_str());
        pipe = popen(Form("ils -l %s%s/ | grep .root | awk '{print $4\"	\"$7}' | sort | uniq",Param.InputFile[1],SUFFIX.c_str()), "r");
//        pipe = popen(Form("ils -l %s/ | grep .root | grep -v diskcache | sort | uniq",Param.InputFile[1]), "r");
    }
    else
      {
        printf("   - Get content of %s%s/ \n",Param.InputFile[1],SUFFIX.c_str());
        pipe = popen(Form("ls -l %s%s/ | grep .root | awk '{print $5\" \"$9}' | sort | uniq",Param.InputFile[1],SUFFIX.c_str()), "r");
      }
    
    fList existingList;
    fList::iterator itFind, oldFind;
    std::vector<int> RID;
    
    unsigned int NirodsFile=0, NLine=0;
    if(pipe != NULL)
    {
        char a[10], c[10], d[10], e[10], buffer[128];
        int b, size, runID, Nevent;
        
        printf("   - Get List of exisiting file in %s/ \n",Param.InputFile[1]);
	char string[100];
        while(!feof(pipe))
        {
            int _READstatus;
            NLine++;
	    
	    fgets(string,100,pipe);
	    runID=0, Nevent=1;
	    
	    //	    printf("Line : %s",string);
            if(_irods)
	    	_READstatus = fscanf(pipe,"%i	%i%*[.]%*[0]%i%*[.A-Za-z]",&size,&runID,&Nevent);
                //_READstatus = fscanf(pipe,"%s %i %s %i %s %s %i%*[.]%08i%*[.A-Za-z]",a,&b,c,&size,d,e,&runID,&Nevent);
            else if(_rfdir)
                _READstatus = fscanf(pipe,"%s %i %s %i %s %s %s",a,&b,c,&size,d,e,buffer);
	    else
  	      _READstatus = fscanf(pipe,"%i   %i%*[.]%*[0]%i%*[.A-Za-z]",&size,&runID,&Nevent);

	    if(force)
	    	continue;
		
            if(_READstatus != 3)
            {
                NLine--;
		printf("\033[43;31mCAN NOT READ LINE\033[0m\n");
                continue;
            }
            
//	    printf("%i %i.%08i.root\n",size,runID,Nevent);
//	    if(Nevent <= 0) Nevent=1;
	    
	    if(RID.size() != 0) RID.clear();
	    RID.push_back(runID); RID.push_back(Nevent);
	    itFind = existingList.find( RID );
	    if(itFind != existingList.end())
	    	printf("Element %i.%08i already exists as %i.%08i\n",RID[0],RID[1], itFind->first[0], itFind->first[1]);
	    
            existingList.insert(std::pair<std::vector<int>,int>(RID,size));
            NirodsFile++;
            
        }
        pclose(pipe);
        
        printf("Number of Lines : %u\n",NLine);
        printf("Number of files on ");
        if(_rfdir)
            printf("HPSS : ");
        else if(_irods)
            printf("irods : ");
	else 
	  printf("local : ");
        printf("%i (%u)\n",static_cast<int>(existingList.size()),NirodsFile);
        
    }

   	if(existingList.size() > 0)
		oldFind=existingList.begin();
	
	for(unsigned int f=0; f<ifile.size(); f++)
	{
        if(pData->IsBadRun(ifile[f]))
			continue;
        
        if(Param.STARTAT > 0 && LxAMSdata::GetRun(ifile[f]) < Param.STARTAT)
            continue;
        
        if(Param.ENDAT > 0 && LxAMSdata::GetRun(ifile[f]) > Param.ENDAT)
            continue;
        
        //if(_rfdir || _irods)
        {
            RID.clear();
            if(existingList.size() == 0 || force)
            {
                //---> No file matches, add file to job failed list
                failedList.push_back(string(ifile[f].c_str()));
                if(!silent) printf("%s \033[31m[MISSING FILE]\033[0m\n",failedList[failedList.size()-1].c_str());
                Nmiss++;
                continue;
            }
            
            RID.push_back(static_cast<int>(LxAMSdata::GetRun(ifile[f])));
            RID.push_back(static_cast<int>(LxAMSdata::GetFirstEvt(ifile[f])));
            itFind = existingList.find( RID );
	    
            if(itFind == existingList.end())
            {
                
				//printf("-------------------\nFile %s NOT FOUND\n",ifile[f].c_str());
				//gSystem->Exec(Form("ils %s/%i.%08i.root",Param.InputFile[1],static_cast<int>(LxAMSdata::GetRun(ifile[f])),static_cast<int>(LxAMSdata::GetFirstEvt(ifile[f]))));
				bool match=false;
				itFind=oldFind;
                while(!match && itFind != existingList.end())
				{
				    if(itFind->first[0] == LxAMSdata::GetRun(ifile[f]) && itFind->first[1] == LxAMSdata::GetFirstEvt(ifile[f]))
		    		match = true;
		
				    if(!match)
			    	itFind++ ;
				}
                
				if(!match)
                {
                	//---> No file matches, add file to job failed list
                	failedList.push_back(string(ifile[f].c_str()));
                	if(!silent) printf("%s \033[31m[MISSING FILE]\033[0m\n",failedList[failedList.size()-1].c_str());
                	Nmiss++;
					continue;
				}
		
            }
            
	    /*            if(itFind->second < 10205)
            {
                failedList.push_back(string(ifile[f].c_str()));
                if(!silent) printf("%s \033[31m[TREE MISSING OR EMPTY]\033[0m\n",failedList[failedList.size()-1].c_str());
                Ncorr++;
				oldFind=itFind;
				continue;
	     }*/
            
			if(Param.openIrods)
			{
				gSystem->Exec(Form("iget %s/%i.%08i.root",Param.InputFile[1],itFind->first[0],itFind->first[1]));
				if(!silent) printf("OPEN FILE %i.%08i.root\n",itFind->first[0],itFind->first[1]);
				rootFile = TFile::Open(Form("%i.%08i.root",itFind->first[0],itFind->first[1]));
				
				if(rootFile == NULL)
				{
					failedList.push_back(string(ifile[f].c_str()));
					if(!silent) printf("%s \033[31m[FILE CORRUPTED]\033[0m\n",failedList[failedList.size()-1].c_str());
					Ncorr++;
					oldFind=itFind;
					gSystem->Exec(Form("rm -f %i.%08i.root",itFind->first[0],itFind->first[1]));
					continue;
				}
				
				if(rootFile->IsZombie())
				{
					failedList.push_back(string(ifile[f].c_str()));
					if(!silent) printf("%s \033[31m[FILE MADE ZOMBIE]\033[0m\n",failedList[failedList.size()-1].c_str());
					Ncorr++;
					oldFind=itFind;
					rootFile->Close();
					gSystem->Exec(Form("rm -f %i.%08i.root",itFind->first[0],itFind->first[1]));
					continue;
				}
				
				RecEvent=(TTree*) rootFile->Get("RecEvent");
				
				if(RecEvent==NULL)
				{
					failedList.push_back(string(ifile[f].c_str()));
					if(!silent) printf("%s \033[31m[TREE MISSING OR EMPTY]\033[0m\n",failedList[failedList.size()-1].c_str());
					Ncorr++;
					oldFind=itFind;
					rootFile->Close();
					gSystem->Exec(Form("rm -f %i.%08i.root",itFind->first[0],itFind->first[1]));
					continue;
				}
				
				bool completed=false;
				TList *userInfo=RecEvent->GetUserInfo();
				TIter it((TList*) userInfo);
				TObject *obj;
				
				while ((obj = it()))
				{
					if(strstr(((TObjString*) obj)->GetString().Data(),"CPU") != NULL)
						completed=true;
				}
				
				if(!completed)
				{
					failedList.push_back(string(ifile[f].c_str()));
					if(!silent) printf("%s \033[31m[TREE INCOMPLET]\033[0m\n",failedList[failedList.size()-1].c_str());
					Ncorr++;
				}
				
				rootFile->Close();
				gSystem->Exec(Form("rm -f %i.%08i.root",itFind->first[0],itFind->first[1]));
				
			}
			
	   		oldFind=itFind;
            continue;
        }
		
		rootFile = TFile::Open(Form("%s/%s%s-%i.%08i.root",Param.InputFile[1],PREFIX.Data(),SCRIPT.Data(),LxAMSdata::GetRun(ifile[f]),LxAMSdata::GetFirstEvt(ifile[f])));
		if(rootFile == NULL)
			rootFile = TFile::Open(Form("%s/%i.%08i.root",Param.InputFile[1],LxAMSdata::GetRun(ifile[f]),LxAMSdata::GetFirstEvt(ifile[f])));
        
       		if(rootFile == NULL)
		{
			//---> No file matches, add file to job failed list
			failedList.push_back(string(ifile[f].c_str()));
			if(!silent) printf("%s \033[31m[MISSING FILE]\033[0m\n",failedList[failedList.size()-1].c_str());
			Nmiss++;
			continue;
		}
		
		if(rootFile->IsZombie())
		{
			failedList.push_back(string(ifile[f].c_str()));
			if(!silent) printf("%s \033[31m[ZOMBIE]\033[0m\n",failedList[failedList.size()-1].c_str());
			Ncorr++;
		}
		
		if (rootFile->TestBit(TFile::kRecovered))
		{
			failedList.push_back(string(ifile[f].c_str()));
			if(!silent) printf("%s \033[31m[RECOVERED]\033[0m\n",failedList[failedList.size()-1].c_str());
			Ncorr++;
			rootFile->Close();
			continue;
		}
		
		
		RecEvent = (TTree*) rootFile->Get("RecEvent");
		
		if(RecEvent == NULL)
		{
			failedList.push_back(string(ifile[f].c_str()));
			printf("%s \033[31m[MISSING TREE]\033[0m\n",failedList[failedList.size()-1].c_str());
			Ncorr++;
		}
		else if(RecEvent->GetEntries() <= 0)
		{
			failedList.push_back(string(ifile[f].c_str()));
			printf("%s \033[31m[EMPTY TREE]\033[0m\n",failedList[failedList.size()-1].c_str());
			Ncorr++;
		}			
		
		rootFile->Close();
	}
	
	ifile.clear();

	printf("\n\n\033[22;31m--------------  %s  ---------------\n\033[22;34mSUMMARY\033[0m.\n",PREFIX.Data());
	printf("Number of source file    : %u\n", Nfile);
	
	if(_irods || _rfdir )
		printf("Number of tree file      : %i\n",static_cast<int>(existingList.size()));
		
	printf("Number of Missing file   : %u [%.3f %%]\n", Nmiss,(float) Nmiss/(float)Nfile*100.f);
	printf("Number of Corrupted file : %u [%.3f %%]\n", Ncorr,(float) Ncorr/(float)Nfile*100.f);
	printf("\033[22;31m-----------------------------\033[0m\n\n");
	
	FILE * pFile;

	int splitting = 0;
	unsigned int iJob=0;

	while(splitting < nsplit)
	{

	  if(iJob >= failedList.size())
	    break;
	
	  if(nsplit)
	    {
	      printf("CREATE FILE : %s/SCRIPT/%s%s-%i.GE\n",gSystem->ExpandPathName("$HOME"), PREFIX.Data(),SCRIPT.Data(),splitting);
	    pFile = fopen (Form("%s/SCRIPT/%s%s-%i.GE",gSystem->ExpandPathName("$HOME"),PREFIX.Data(),SCRIPT.Data(),splitting),"w");
	    }
	  else
	    {
	      printf("CREATE FILE : %s/SCRIPT/%s%s.GE\n",gSystem->ExpandPathName("$HOME"),PREFIX.Data(),SCRIPT.Data());
	    pFile = fopen (Form("%s/SCRIPT/%s%s.GE",gSystem->ExpandPathName("$HOME"),PREFIX.Data(),SCRIPT.Data()),"w");
	    }
    
	
	if(pFile == NULL)
	{
		printf("CAN'T CREATE %s/%s%s.GE \n",gSystem->ExpandPathName("$HOME"),PREFIX.Data(),SCRIPT.Data());
		return -1;
	}
	
	fprintf(pFile,"#!/bin/bash \n");
	fprintf(pFile,"# SCRIPT AUTOMATICALLY GENERATED. DO NOT MODIFY\n# ALL MODIFICATION WILL BE REMOVED DURING NEXT EXECUTION.\n\n\n");

	fprintf(pFile,"export HOME=%s\n",gSystem->ExpandPathName("$HOME"));

	fprintf(pFile,"if [ -f $HOME/AMS-ANA/%s.C ]; then\n",SCRIPT.Data());
	fprintf(pFile,"		cp $HOME/AMS-ANA/%s.* $HOME/AMS-ANA/SCRIPT/. \n",SCRIPT.Data());
	fprintf(pFile,"fi\n\n\n");
	
	unsigned int ixPrep=0;
	unsigned int next=Param.NFile;
	
	while(iJob < failedList.size())
	{
	  if(nsplit > 0 && iJob >= static_cast<int>( failedList.size() )/nsplit*(splitting+1))
	    {
	      printf("%i  -> %i      (%i)\n",iJob, static_cast<int>( failedList.size() )/nsplit*(splitting+1), static_cast<int>( failedList.size() ));
	      splitting++;
	      break;
	    }
		next=iJob+Param.NFile;

		if(Param.useIrods)
			fprintf(pFile,"qsub -P P_ams -N %s%s-%u -o /sps/ams/lpsc/william/LOG/%s%s-%u.log -p -5 -j y -q %s -l cvmfs=1,sps=1,irods=1,xrootd=1,fsize=%s,vmem=%s,os=sl6 -M %s@lpsc.in2p3.fr -m s <<!\n",PREFIX.Data(),SCRIPT.Data(),iJob,PREFIX.Data(), SCRIPT.Data(),iJob,Param.queue,Param.fsize,Param.vmem,gSystem->ExpandPathName("$USER"));
		else
			fprintf(pFile,"qsub -P P_ams -N %s%s-%u -o /sps/ams/lpsc/william/LOG/%s%s-%u.log -p -5 -j y -q %s -l cvmfs=1,sps=1,xrootd=1,fsize=%s,vmem=%s,os=sl6 -M %s@lpsc.in2p3.fr -m s <<!\n",PREFIX.Data(),SCRIPT.Data(),iJob,PREFIX.Data(), SCRIPT.Data(),iJob,Param.queue,Param.fsize,Param.vmem,gSystem->ExpandPathName("$USER"));
		
		fprintf(pFile,"#!/bin/bash \n\n");

		fprintf(pFile,"export HOME=%s\n",gSystem->ExpandPathName("$HOME"));
				
		fprintf(pFile,"export AMS_VER=vdev\n");
		fprintf(pFile,"source $HOME/setAMS.sh\n");
		fprintf(pFile,"export AMSDataDir=/cvmfs/ams.cern.ch/Offline/AMSDataDir\n");
		fprintf(pFile,"export AMSDataDir_v=/cvmfs/ams.cern.ch/Offline/AMSDataDir/v5.00\n\n");
		
		fprintf(pFile,"#--- USINE++ environment\n");
		fprintf(pFile,"export USINE=$HOME/PROG/$PROG_BIT/USINE/TRUNK\n\n");
		
		fprintf(pFile,"# Default directories of data, parameter files, input and output\n");
		fprintf(pFile,"# (It's up to the user to define his own path)\n");
		fprintf(pFile,"export USINE_DATA=$USINE/data\n");
		fprintf(pFile,"export USINE_PFILE=$USINE/parfile\n");
		fprintf(pFile,"export USINE_INPUT=$USINE/input\n");
		fprintf(pFile,"export USINE_OUTPUT=$USINE/output\n");
		fprintf(pFile,"export USINE_MCMC=$USINE/output\n\n");
		
//		fprintf(pFile,"#--- CRsimD environment\n");
//		fprintf(pFile,"export CRSIMDROOT=$GROUP_DIR/Offline/CRSIMD\n\n");
		
		fprintf(pFile,"export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CRSIMDROOT}/LIBS\n");
		fprintf(pFile,"echo UPDATE LIBRARY PATH TO $LD_LIBRARY_PATH\n\n");
		
		fprintf(pFile,"echo \033[31m-----------------------------------\033[0m\n");
		fprintf(pFile,"echo LXSOFT     : ${LXSOFT}\n");
		fprintf(pFile,"echo CRSIMDROOT : ${CRSIMDROOT}\n");
		fprintf(pFile,"echo ROOTSYS    : ${ROOTSYS}\n\n");
		fprintf(pFile,"echo \033[31m-----------------------------------\033[0m\n\n");
		
		
		fprintf(pFile,"export PATH=${PATH}:${LXSOFT}/BIN/${AMS_VER}:${ROOTSYS}/bin \n\n");
//		fprintf(pFile,"export LOCDIR=$TMPDIR \n\n");
//		fprintf(pFile,"echo WORKING DIR $LOCDIR\n\n");
//		fprintf(pFile,"mkdir ./proofSession\n\n");
		
		fprintf(pFile,"#COPY FILE IN THE BATCH CWD\n\n");
		
		fprintf(pFile,"export SOURCE=%s \n",SCRIPT.Data());
		
		fprintf(pFile,"# 1- COPY SCRIPT FILE\n\n");
		fprintf(pFile,"if [ -f ./%s.C ]; then\n	echo File %s.C already exist\n else\n	if [ -f $HOME/AMS-ANA/SCRIPT/%s.C ]; then\n		cp $HOME/AMS-ANA/SCRIPT/%s.C .\n	else\n		if [ -f $HOME/AMS-ANA/%s.C ]; then\n			cp $HOME/AMS-ANA/%s.C $HOME/AMS-ANA/SCRIPT/.\n			cp $HOME/AMS-ANA/SCRIPT/%s.C .\n		else\n			echo %s.C file not found.\n			exit 127\n		fi\n	fi\nfi\n\n",SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data());
					
		fprintf(pFile,"# 2- COPY HEADER SCRIPT FILE\n");
		fprintf(pFile,"if [ -f ./%s.h ]; then\n	echo File %s.h already exist\nelse\n	if [ -f $HOME/AMS-ANA/SCRIPT/%s.h ]; then\n		cp $HOME/AMS-ANA/SCRIPT/%s.h .\n	else\n		if [ -f $HOME/AMS-ANA/%s.h ]; then\n			cp $HOME/AMS-ANA/%s.h .\n		fi\n	fi\nfi\n\n",SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data());
		
		if(upTree)
		{
			fprintf(pFile,"# 2.1- COPY TREE UPDATE \n");
			fprintf(pFile,"if [ -f $LXSOFT/BIN/$AMS_VER/TreeUpdate ]; then \n	echo COPY TREE UPDATE \n	cp $LXSOFT/BIN/$AMS_VER/TreeUpdate . \n fi \n\n");
		} 
		
		fprintf(pFile,"# 3- COPY MAIN EXECUTABLE\n");
		fprintf(pFile,"if [ -f AMSAnalyser.cxx ]; then\n	echo File AMSAnalyser.cxx already exist\nelse\n	if [ -f $HOME/AMS-ANA/AMSAnalyser.cxx ]; then\n		cp $HOME/AMS-ANA/AMSAnalyser.cxx .\n	else\n		echo AMSAnalyser.cxx file not found.\n		exit 127\n	fi\nfi\n\n");
																
		fprintf(pFile,"if [ -f AMSAnalyser.h ]; then\n	echo File AMSAnalyser.h already exist\nelse\n	if [ -f $HOME/AMS-ANA/AMSAnalyser.h ]; then\n		cp $HOME/AMS-ANA/AMSAnalyser.h .\n	else\n		echo AMSAnalyser.h file not found.\n		exit 127\n	fi\nfi\n\n");
														
		fprintf(pFile,"if [ -f Makefile ]; then\n	echo File Makefile already exist\nelse\n	if [ -f $HOME/AMS-ANA/Makefile ]; then\n		cp $HOME/AMS-ANA/Makefile .\n	else\n		echo Makefile file not found.\n		exit 127\n	fi\nfi\n\n\n make all\n\n\n");
		
		if(Param.copyIrods)
		{
			fprintf(pFile,"if [ ! -d ""INPUT"" ]; then\n");
			fprintf(pFile,"		mkdir INPUT\n");
			fprintf(pFile,"fi\n");
		}
		else
		{
			while (ixPrep < next)
			{
				if(ixPrep >= failedList.size())
					break;
				
				fprintf(pFile,"xrdstagetool -s -p root://ccxroot:1999//hpss/in2p3.fr/group/%s\n",failedList[ixPrep].c_str());
				ixPrep++;
			}
		}
		
		fprintf(pFile,"\n\n");

		int firstRun =0;
		int LastRun  =0;

		while (iJob < next)
		{
			if(iJob >= failedList.size())
				break;

			int RunNumber = LxAMSdata::GetRun		(failedList[iJob]);
			int Event     = LxAMSdata::GetFirstEvt	(failedList[iJob]);

			if(firstRun == 0)
				firstRun = RunNumber;
			else
				LastRun = RunNumber;

			std::string filePath(failedList[iJob].c_str());
			filePath.replace(failedList[iJob].find_last_of("//"),2,"/");

                        filePath.resize(failedList[iJob].find_last_of("/"));

			fprintf(pFile,"\n\n#-----------------\n#	RUN ANALYSES\n");
			fprintf(pFile,"echo \033[31m----------------- \n");
			fprintf(pFile,"echo \033[31m    `date ` : FILE PATH %s\033[0m\n",filePath.c_str());
			fprintf(pFile,"echo \033[31m	`date ` : RUN ANALYSES on file root://ccxroot:1999//hpss/in2p3.fr/group/%s [RUN : %i]\033[0m\n",failedList[iJob].c_str(),RunNumber);

			if(Param.copyIrods && !upTree)
			{
			        fprintf(pFile,"iget %s/%i.%08i.root \n",filePath.c_str(),RunNumber,Event);
				fprintf(pFile,"mv %i.%08i.root INPUT/.\n",RunNumber,Event);
				
				fprintf(pFile,"if [ -f INPUT/%i.%08i.root ]; then\n",RunNumber,Event);
				fprintf(pFile,"	echo  \033[34mAMSAnalyser.exe -OutputFile %s%s-%i.%08i.root %s -tag local %s INPUT/%i.%08i.root \033[0m\n",PREFIX.Data(), SCRIPT.Data(), RunNumber,Event,Param.OPTIONS,Param.Script,RunNumber,Event);
				fprintf(pFile,"	./AMSAnalyser.exe -OutputFile %s%s-%i.%08i.root %s -tag local %s INPUT/%i.%08i.root \n\n",PREFIX.Data(), SCRIPT.Data(), RunNumber,Event,Param.OPTIONS,Param.Script,RunNumber,Event);
				
				fprintf(pFile,"	rm -f INPUT/%i.%08i.root\n",RunNumber,Event);
				fprintf(pFile,"fi\n\n");
			}
			else if(!upTree)
				fprintf(pFile,"./AMSAnalyser.exe -OutputFile %s%s-%i.%08i.root %s %s root://ccxroot:1999//hpss/in2p3.fr/group/%s \n\n",PREFIX.Data(), SCRIPT.Data(), RunNumber,Event,Param.OPTIONS,Param.Script,failedList[iJob].c_str());
			else
			{
				fprintf(pFile,"./TreeUpdate -irods %s %s.C root://ccxroot:1999//hpss/in2p3.fr/group/%s %s/%i.%08i.root \n\n",Param.OPTIONS,SCRIPT.Data(),failedList[iJob].c_str(),Param.InputFile[1],RunNumber,Event);
				fprintf(pFile,"echo '\033[31m<<<<<< TreeUpdate COMPLETED\033[0m'\n echo \n\n");
				
				fprintf(pFile,"if [ -f %s.root ]; then \n",SCRIPT.Data());
				fprintf(pFile,"	$HOME/AMS-ANA/./CheckFile.exe -tree +uptree %s.root \n",SCRIPT.Data());
				fprintf(pFile,"fi\n\n");
				
				fprintf(pFile,"if [ -f %s.root ]; then \n",SCRIPT.Data());

				if(mergeUp)
				{
					fprintf(pFile,"	echo '\033[31m>>>>>> '`date `': COPY old Tree to working dir '`pwd`' \033[0m' \n");
					fprintf(pFile,"	xrdcp root://ccxroot:1999//hpss/in2p3.fr/group/%s/%i.%08i.root OldTree.root \n",Param.InputFile[1],RunNumber,Event);
					fprintf(pFile,"	echo '\033[31m<<<<<< DONE\033[0m'\n		echo \n\n");
				
					fprintf(pFile,"	echo '\033[31m>>>>>> '`date `': MERGE Old and Updated Tree \033[0m' \n");
					fprintf(pFile,"	hadd -f  %i.%08i.root  %s.root OldTree.root\n",RunNumber,Event,SCRIPT.Data());
					fprintf(pFile,"	echo '\033[31m<<<<<< DONE\033[0m'\n		echo \n\n");
				}
				else
				{
					fprintf(pFile,"	cp %s.root upTree-%i.%08i.root\n",SCRIPT.Data(),RunNumber,Event);
					fprintf(pFile,"	mv %s.root %i.%08i.root\n\n",SCRIPT.Data(),RunNumber,Event);
				}
				
				fprintf(pFile,"	if [ -f %i.%08i.root ]; then \n",RunNumber,Event);
				fprintf(pFile,"		echo '\033[31m>>>>>> '`date `': IPUT MERGED FILE TO %s_update/%i.%08i.root \033[0m' \n",Param.InputFile[1],RunNumber,Event);
				fprintf(pFile,"		iput -f %i.%08i.root  %s_update/%i.%08i.root \n",RunNumber,Event,Param.InputFile[1],RunNumber,Event);
				fprintf(pFile,"		echo '\033[31m<<<<<< DONE\033[0m'\n 		echo \n");
				fprintf(pFile,"	fi \n\n");
				
				fprintf(pFile,"else \n");
				fprintf(pFile,"	echo '\033[43;31mUpTree.root file HAVE NOT BEEN PRODUCED\033[0m'\n");
				fprintf(pFile,"fi \n");
				
				fprintf(pFile,"if [ -f %s.root ]; then \n echo +CLEAR %s.root \n rm -f %s.root \n echo -DONE \n fi \n echo \n\n",SCRIPT.Data(),SCRIPT.Data(),SCRIPT.Data());
				fprintf(pFile,"if [ -f UpTree.root ]; then \n echo +CLEAR UpTree.root \n rm -f UpTree.root \n echo -DONE \n fi \n echo \n\n");
				fprintf(pFile,"if [ -f OldTree.root ]; then \n echo +CLEAR OldTree.root \n rm -f OldTree.root \n echo -DONE \n fi \n echo \n\n");
				fprintf(pFile,"if [ -f %i.%08i.root ]; then \n echo +CLEAR %i.%08i.root \n rm -f %i.%08i.root \n echo -DONE \n fi \n echo \n\n",RunNumber,Event,RunNumber,Event,RunNumber,Event);
				fprintf(pFile,"echo '+CLEAR file*' \n rm -f file\*\n\n");
				fprintf(pFile,"echo '+CLEAR proof residual files' \n rm -rf proof-\* plite-\* \n\n");
			}
			
			if(!upTree)
			{
				fprintf(pFile,"\n#	CHECK INTEGRITY OF THE ROOT FILE\n");
				fprintf(pFile,"if [ -f %s%s-%i.%08i.root ]; then\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event);
				if(madeTree)
					fprintf(pFile,"	$HOME/AMS-ANA/./CheckFile.exe %s%s-%i.%08i.root\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event);
				else
					fprintf(pFile,"	$HOME/AMS-ANA/./CheckFile.exe -tree %s%s-%i.%08i.root\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event);
				fprintf(pFile,"fi\n\n");
			
				fprintf(pFile,"#	COPY OUTPUT\n");
				fprintf(pFile,"if [ -f %s%s-%i.%08i.root ]; then\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event);
            
				if(_irods)
            	{
                	fprintf(pFile,"	echo iput %s%s-%i.%08i.root %s\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event,Param.InputFile[1]);
                	fprintf(pFile,"	iput -k -f %s%s-%i.%08i.root %s/%i.%08i.root\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event,Param.InputFile[1],RunNumber,Event);
            	}
            	else
            	{
            		fprintf(pFile,"	echo COPY %s%s-%i.%08i.root %s/%i.%08i.root\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event,Param.InputFile[1],RunNumber,Event);
            		fprintf(pFile,"	cp %s%s-%i.%08i.root %s/%i.%08i.root\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event,Param.InputFile[1],RunNumber,Event);
					if(madeTree)
						fprintf(pFile, " $HOME/ANALYSER/MACRO/./AddDatacard.exe %s %s/%i.%08i.root\n", filePath.c_str(),Param.InputFile[1],RunNumber,Event);
            	}

				fprintf(pFile,"	rm -f %s%s-%i.%08i.root*\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event);
				fprintf(pFile,"else\n");
				fprintf(pFile,"       cp %s%s-%i.%08i.LOG /sps/ams/lpsc/william/LOG/.\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event);
				fprintf(pFile,"fi\n\n");
				fprintf(pFile,"rm -f %s%s-%i.%08i.LOG\n",PREFIX.Data(),SCRIPT.Data(),RunNumber,Event);
			}

			
			iJob ++;
			
		}

		if(!mergeUp && spscopy && upTree)
		{
			fprintf(pFile,"	hadd -f  %i.%i.root  upTree-*.root\n",firstRun,LastRun);

			int _pose = std::string(Param.InputFile[1]).find_last_of("/");
			fprintf(pFile,"	if [ -f  %i.%i.root ] then; cp %i.%i.root /sps/ams/lpsc/tree/B620.%.*s_update\n",firstRun,LastRun,firstRun,LastRun,strlen(Param.InputFile[1])-_pose-1, Param.InputFile[1]+_pose+1);
		}

		fprintf(pFile,"!\n\n\n#---------------------------------------------------------\n");
		
		if(iJob >= failedList.size())
			break;
		
		
	}

	fclose(pFile);
	
	if(nsplit == 0)
	  break;
	
	}

	return 0;
}
