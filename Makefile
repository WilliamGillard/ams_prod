
SrcSuf        = cxx
ScriptSuf     = C 
IncSuf        = h

COMP = g

MACHINE := $(shell uname -m)-$(shell uname -s)
ifeq ($(COMP),g)
MACHINE := $(MACHINE)-debug
endif

ifneq ($(GFORTRANSYS),)
GFFLAGS = -L$(GFORTRANSYS)/lib
else
GFFLAGS =
endif

LibDir = .

BinDir = .

ROOTCFLAGS   := $(shell root-config --cflags)
ROOTLIBS     := $(shell root-config --libs --glibs) -lMinuit -lEG -lNetx -lGeom -lEG
LIBCONFIG    := -L$(AMSWD)/lib/$(shell root-config --arch) -lntuple_slc4_PG

ifeq ($(shell echo $(AMS_VER) | tr -dc '0-9' | awk '{if($$1>=530){ print "YES"} else {print "NO"}}'),YES)
	ROOTLIBS += -lTMVA -lXMLIO -lMLP -lTree
endif

CERNlib := $(shell $(CERNDIR)/bin/cernlib geant321 pawlib lapack3 blas graflib mathlib grafX11 packlib kernlib)

INCFLAGS     := -I$(shell $(ROOTSYS)/bin/root-config --incdir)
INCFLAGS     += -isystem $(AMSSRC)/include

MACH := $(shell uname)

CXX           = g++
LD            = g++
AR            = ar r
CXXFLAGS      =  -Wall -fPIC -$(COMP)  -D$(MACH) -ffast-math -Wno-pragmas 
CCFLAGS	      =  -Wall -fPIC -$(COMP) -ffast-math  -Wno-pragmas
SOFLAGS       = -shared 

ifeq ($(MACH),Darwin)
SOFLAGS       =  -dynamiclib -single_module -undefined dynamic_lookup
endif

CXXFLAGS     += $(ROOTCFLAGS) -trigraphs 
LIBS          = $(ROOTLIBS) 

GCCVERSIONGT3 = $(shell gcc --version | head -1 | awk -F"[ ]" '{ if($$3>3.9) { print "YES"} else {print "NO"}}')
ifeq ($(GCCVERSIONGT3),YES)
LIBS          +=$(GFFLAGS) -lgfortran
else
LIBS          +=-lg2c
endif

LIBS += -L/afs/in2p3.fr/system/amd64_sl6/usr/local/products/xrootd/root/3.3.2/lib64 -lXrdClient

locallibslink := -L$(LXSOFT)/LxAMS/LIBS/$(AMS_VER) -lLxBase_ -lLxAna_ -lLxGeomag_ -lLxEvD_ -lLipRec_ \
	-L$(CRSIMDROOT)/LIBS/ -lCRgeomag -L$(LXSOFT)/ASTROLIB/LIB -lAstro_ -lTwAMS_ $(CERNlib) -L$(AMSWD)/lib/$(shell root-config --arch) -lntuple_slc4_PG
#----------------------------------------------------------------------------
Exe = AMSAnalyser.exe

Lib =
lobjects  = $(patsubst %,%.o,$(Lib))
lname     = $(patsubst %,lib%.so,$(Lib))
dictionary= $(patsubst %,%_Dict.o,$(Lib))


cxxobjects = AMSAnalyser.o
Cobjects   = 

CXXFLAGS      += $(INCFLAGS)

#----------------------------------------------------------------------------
LIBS          += $(LIBCONFIG)

#----------------------------------------------------------------------------
all:  $(Exe) $(Lib)
exe: $(Exe)
force: distclean all
job : JobResub.exe CheckFile.exe
JobResub: JobResub.exe
CheckFile: CheckFile.exe
#----------------------------------------------------------------------------

.SUFFIXES: .$(SrcSuf) %.$(IncSuf) .d  %.$(ScriptSuf)


%.d: %.$(SrcSuf) %.$(ScriptSuf)  %.$(IncSuf) 
	@echo "construct $(@F)"	
	@$(SHELL) -ec '$(CXX) -MM $(CXXFLAGS)  $<  -MT '$(LibDir)/$*.o' | sed '\''s/\($*\)\.o[ :]*/\1.o $(@F) : /g'\'' > $(@F); [ -s $(@F) ] || rm -f $(@F)'

%.o: %.$(SrcSuf)
	@echo ""
	@echo -e $(GREEN)"Construct $(@F)"	$(Reset)
	$(CXX) $(CXXFLAGS) $(INCFLAGS) -I$(LXSOFT)/LxAMS/LxTools -I$(LXSOFT)/LxAMS/LxGeomag -I$(LXSOFT)/LxAMS/LxBase -I$(LXSOFT)/LxAMS/LxAna -I$(LXSOFT)/LxAMS/LxEvD -I$(CRSIMDROOT)/geomag -I$(LXSOFT)/ASTROLIB/include -c $< -o $(@F)
	@echo ""

$(cxxobjects):$(LibDir)/%.o: %.$(SrcSuf)
	@$(CXX) $(CXXFLAGS) $(INCFLAGS) -I$(LXSOFT)/LxAMS/LxTools -I$(LXSOFT)/LxAMS/LxGeomag -I$(LXSOFT)/LxAMS/LxBase -I$(LXSOFT)/LxAMS/LxAna -I$(LXSOFT)/LxAMS/LxEvD -I$(CRSIMDROOT)/geomag -I$(LXSOFT)/ASTROLIB/include -c $< -o $@
	@echo "$@ done..."

$(Cobjects):$(LibDir)/%.o: %.$(ScriptSuf)
	@$(CXX) $(CXXFLAGS) -I$(LXSOFT)/LxAMS/LxTools -I$(LXSOFT)/LxAMS/LxGeomag -I$(LXSOFT)/LxAMS/LxBase -I$(LXSOFT)/LxAMS/LxAna -I$(LXSOFT)/LxAMS/LxEvD -I$(CRSIMDROOT)/geomag -I$(LXSOFT)/ASTROLIB/include -c $< -o $@

$(Lib): % : $(cxxobjects) $(lobjects)
	@echo ""
	@echo  -e $(VIOLET)"Build $(@F)" $(Reset)
	$(LD) $(SOFLAGS) -o $(LibDir)/lib$(@F).so $(@F).o  $(LIBS)

$(BinDir)/%.exe: %.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(LIBS) $(locallibslink) -lpthread
	@echo "$@ done..."
	@echo

$(BinDir)/%.exec: %.o
	$(CXX) $(CXXFLAGS) $< -o $@ $(LIBS) $(locallibslink)
	@echo "$@ done..."
	@echo

-include $(ldep)

clean:
	rm -f *.o *.d

distclean:
	rm -f $(Exe) *.o *.d


