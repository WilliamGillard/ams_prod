#ifndef _PGTRACK_
#define _PGTRACK_
#endif

#pragma mark - Include

// AMS
#include <root.h>
#include <TrdKCluster.h>
#include <TofRecon.h>
#include <RichCharge.h>
#include <EcalChi2CY.h>
#include <TrRecon.h>
#include <bcorr.h>

// SYSTEM
#include <fstream>
#include <math.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

// ROOT
#include <TRandom.h>
#include <TBranchElement.h>
#include <TROOT.h>
#include <TTree.h>
#include <TClassEdit.h>
#include <TObject.h>
#include <TList.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TApplication.h>
#include <TString.h>
#include <TFile.h>
#include <TClass.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TH2D.h>
#include <TLatex.h>
#include <TPad.h>
#include <TPaveLabel.h>
#include <TChain.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TLine.h>
#include <TStyle.h>
#include <TF1.h>
#include <TH1F.h>
#include <TH2D.h>
#include <TF2.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TMatrix.h>
#include <TRandom.h>

// LXSOFT
#include <Astrolib.h>
#include <LxGeomag.h>
#include <LxTreeUpdate.h>
#include <LxSelector.h>

#include <EcalHadron.h>

typedef map<TString, ChargeSubDR>::const_iterator MapIterator;

#ifndef MICROSEC
#define MICROSEC 1000000
#endif

#pragma mark stat_m structure definition
//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
// PROCESS INFO STRUCTURE
//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
typedef struct
{
    int pid, ppid, pgrp, session, tty_nr, tpgid, exit_signal, processor;
	unsigned long flag, minflt, cminflt, majflt, cmajflt, utime, stime, strattime, vsize, rlim, startcode, endcode, startstack, kstkeep, kstkeip, signal, blocked, sigignore, sigcatch, wchan, nswap, cnswap, rt_prior, policy;
	long int cutime, cstime, priority, nice, num_thread, itrealvalue, rss;
	unsigned long long delayed;
	char comm[80], state;
	
} stat_t;


//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
// CLASS DEFINITION
//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
#pragma mark - PM5Trigger class definition

class PM5Trigger : public LxTreeUpdate
{
	public :
	
	PM5Trigger();
	
	~PM5Trigger();
	
#ifdef __CINT__
#include <process_cint.h>
#endif
	
	void    LxBegin();
	void	LxResetEventTree();
	bool    LxParticle();
	bool    LxProcessCut();
	void    LxProcessFill();
	void    LxTerminate();
	void	LxNotify();
	
	bool isBeamTest;
	
	private :
	
#pragma mark → DEFAULT VARIABLE
	//---- LIST OF DEFAULT VARIABLE ----//
	// This variable should not be removed or modified, unlesss you perform
	// heavy modification of the default code.
	
	bool flag;
	bool debug;
	bool cloneEvt;

	bool refit;
	bool getCoo;

	int TRK_PM4;
	TrTrackR *tr_track;
	
	TTree *Copy;

	float TRK_PM5Rig;
        float TRK_Rigidity;
	int   TRK_isInParticle;

        float TRK_PM5HitCoo[9][3];
  
	int TOF_BetaPattern;
  int NToFClusterInTime;
};

#pragma mark - Constructor
//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
// Constructor
//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
PM5Trigger::PM5Trigger()
{
    LxTreeUpdate::className="PM5Trigger";
    DynAlManager::SetTDVName("DynAlignmentV5T140713PM5");

    setMethod(LxTreeUpdate::LxUSelection);

	return;
}

#pragma mark - Destructor
//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
// Destructor
//•••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
PM5Trigger::~PM5Trigger()
{
	return;
}

#pragma mark - Initialisation


//••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
// Finalaize intitilization with the creation of a TGraph to monitor memory usage
// Adding new branch to the TTree.
void PM5Trigger::LxBegin()
{

#pragma mark → Create graph for Memory managment

	TTree *tt = (TTree*) LxTreeUpdate::GetOutputList()->FindObject("EventUpdate");
	if(tt == NULL)
	{
		printf("\033[43;31m    !!!! ERROR !!!!                              \n    UpdateEvent TTree not found in the memory\033[0m\n");
		exit(0);
	}
	
	tt->Branch("NToFClusterInTime"  , &NToFClusterInTime    ,"NToFClusterInTime/I");
	tt->Branch("TRK_Identity"	, &trkID		,"TRK_Identity/I");
	tt->Branch("TRK_isInParticle"	, &TRK_isInParticle   	,"TRK_isInParticle/b");
	tt->Branch("TRK_PM5Rig"         , &TRK_PM5Rig           ,"TRK_PM5Rig/F");
	tt->Branch("TRK_PM5HitCoo"      , &TRK_PM5HitCoo        ,"TRK_PM5HitCoo[9][3]/F");
	tt->Branch("TOF_BetaPattern"	, &TOF_BetaPattern     	,"TOF_BetaPattern/I");
	
	TkDBc::UseFinal();

	printf("\033[22;31m----<< %s initialization : OK \033[0m \n",LxTreeUpdate::className.Data());
}

void PM5Trigger::LxNotify()
{
  std::string FileName = _Tree->GetCurrentFile()->GetName();
	int _runID = atoi(FileName.substr(FileName.find_last_of("/")+1,10).c_str());

	printf("   \033[34m • CURRENT FILE %s\033[0m\n",FileName.c_str());

}

//••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
// SELECTION CRITERIA
//••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
#pragma mark - Selection
bool PM5Trigger::LxParticle()
{
	bool findPart = LxTreeUpdate::DefaultParticle();

	if(!findPart)
		findPart = LxTreeUpdate::MaxEShower();
	else if(pParticle(0)->iTrTrack() < 0)
		TRK_isInParticle = false;
	else if(trkID == pParticle(0)->iTrTrack())
		TRK_isInParticle = true;

	if(trkID >= 0 && trkID < nTrTrack())
		tr_track = pTrTrack(trkID);

	if(tr_track == NULL)
	{
		refit = false;
		getCoo=false;
		return true;
	}

	TRK_PM4 = tr_track->iTrTrackPar(1,0,1 );
	TRK_Rigidity = tr_track->GetRigidity(TRK_PM4);

	if(TRK_PM4 < 0)
		getCoo = false;

	if((tr_track->GetBitPatternXYJ()&0x101) != 0x101)
		refit = false;

	if((tr_track->GetBitPatternJ()&0x101) != 0x101)
		getCoo = false;

	if(fabs(TRK_Rigidity) < 20.)
	{
		refit = false;
		getCoo=false;
	}

	return true;
}

bool PM5Trigger::LxProcessCut()
{	
	return true;
}

void PM5Trigger::LxProcessFill()
{

	Run		= fHeader.Run;
	Event	= fHeader.Event;

	float bcor1, bcor2;
	int bret1= MagnetVarp::btempcor(bcor1,  0 , 1);
	int bret2= MagnetVarp::btempcor(bcor2,  0 , 2);
	float bFieldCorr = 1;

	if (bret1==0 && bret2==0)
		bFieldCorr = (bcor1+bcor2)/2.;
	else if (bret1 !=0 && bret2 == 0)
		bFieldCorr = bcor2;
	else
		bFieldCorr = 1.;
	
	int ncls[4];
	BetaHR *betaH_tof = NULL;
	betaH_tof = pBetaH(betaHID);
	if(betaH_tof != NULL)
	  NToFClusterInTime = GetNTofClustersInTime(betaH_tof,ncls);

	if(pParticle(0) != NULL)
	{
		if(pParticle(0)->pBeta() != NULL)
			TOF_BetaPattern = pParticle(0)->pBeta()->Pattern;
	}

	if(pParticle(0) != NULL)
	{
		if(pParticle(0)->iTrTrack() < 0)
			TRK_isInParticle = false;
		else if(trkID == pParticle(0)->iTrTrack())
			TRK_isInParticle = true;
	}
	else
		TRK_isInParticle = false;

	if(tr_track == NULL)
		return;

	if(!refit)
		return;

	//--------- APPLY SHIFT ON L1 & L9

       
	// PM5 ALIGNMENT
	int iPM5 = tr_track->iTrTrackPar(1,7,23);

	if(iPM5 >= 0)
		TRK_PM5Rig = tr_track->GetRigidity(iPM5);
	else
		TRK_PM5Rig = 0;

	//———— PM5 HIT COO : !!! PM5 CIEMAT HIT COO IS OBTAINED BY APPLING A SHIFT ON PM5 PG HIT COO
	if(getCoo)
	{
		for(int j=0 ; j < 9; j++)
		{
			if(tr_track->GetHitLJ(j+1) == NULL)
				continue;

			TRK_PM5HitCoo[j][0] = tr_track->GetHitLJ(j+1)->GetCoord(-1, 1).x();
			TRK_PM5HitCoo[j][1] = tr_track->GetHitLJ(j+1)->GetCoord(-1, 1).y();
			TRK_PM5HitCoo[j][2] = tr_track->GetHitLJ(j+1)->GetCoord(-1, 1).z();

		}
	}

	if(Run == 1356913970)
		printf("%u	%.6f	%.6f	%.6f	%.6f	%.6f	PM5 Setup:%i-%s\n",
			   Event,
			   TRK_Rigidity,
			   TRK_PM5Rig,
			   TRK_Rigidity*bFieldCorr,
			   TRK_PM5Rig*bFieldCorr,
			   bFieldCorr,
			   TkDBc::Head->GetSetup (),
			   TkDBc::Head->GetName ()
			   );

}

void PM5Trigger::LxResetEventTree()
{
	TRK_PM4		= -1;

	tr_track = NULL;

	NToFClusterInTime =0;

	TOF_BetaPattern = 0;

	TRK_Rigidity	= 0;
	TRK_PM5Rig	= 0;

	TRK_isInParticle = false;
	
	for(int j = 0; j<9; j++)
	for(int k = 0 ; k<3; k++)
	{
		TRK_PM5HitCoo[j][k]=-500;
	}

	refit  = true;
	getCoo = true;

	return;
}

//••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
// SAVING HISTOGRAMS AND CLONED TREE
//••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
#pragma mark - Terminating
void PM5Trigger::LxTerminate()
{
  	return;

	TTree * _outTree = static_cast<TTree*> ( (static_cast<TTree*>( GetOutputList()->FindObject("EventUpdate") ))->Clone() );
	//if(_outTree != NULL)
	//	_outTree->GetUserInfo()->Add(new TObjString(Form("Updated from PM5Trigger on %i",TTimeStamp().GetDate() )));
	
#pragma mark → Get Current directory
	std::string cCurrentPath = std::string(gSystem->WorkingDirectory());
	std::string workingNode;
	int NODE = -1;
	
	if(cCurrentPath.find("worker") != std::string::npos)
	{
		workingNode  = std::string(cCurrentPath.c_str());
		workingNode.erase(0,workingNode.find_last_of("/")+strlen("worker-0.")+1);
	
		NODE = atoi(workingNode.c_str());
		printf("WORKING NODE : 0.%02i [%s]\n",NODE,workingNode.c_str());
		
		if(NODE > 0)
		{
			TFile *saveTree = new TFile(Form("%s/%s-0.%02i.root",gSystem->Getenv("LOCDIR"),LxTreeUpdate::className.Data(),NODE),"RECREATE");
			if(saveTree != NULL)
			{
				saveTree->Add(_outTree);
				saveTree->Write();
				saveTree->Close();
			}
		}
	}
	else
	{
		printf("WORKING DIR : %s\n",cCurrentPath.c_str());
//		gSystem->Exec(Form("hadd -f %s.root %s-0.*.root",LxTreeUpdate::className.Data(),LxTreeUpdate::className.Data()));
//		gSystem->Exec(Form("rm -f %s-0.*.root",LxTreeUpdate::className.Data()));
	}
	
	printf("\033[22;31m---->> %s Terminate\033[0m \n",LxTreeUpdate::className.Data());
	
	printf("\033[22;31m----<< Success\033[0m \n\n\n");

	while(pthread_self())
	{
		int ret = 100;
		pthread_exit(&ret);
	}

	return;
}
