#!/bin/csh

ls --color=never /sps/ams/lpsc/tree/Lx3.9/mc/protons.B620dev/ | grep -v ecal | grep -v 2004000 | xargs -I % -r sh -c "./JobResub.exe +silent +split -q long -vmem 4G -mc -NFile 25 -p % SelTrigger.C /ams/mc/2011B/protons.B620dev/% /sps/ams/lpsc/tree/Lx3.9/mc/protons.B620dev/%"

ls --color=never /sps/ams/lpsc/tree/Lx3.9/mc/protons.B620dev/ | grep 2004000 | xargs -I % -r sh -c "./JobResub.exe +silent +split -q long -vmem 4G -mc -NFile 200 -p % SelTrigger.C /ams/mc/2011B/protons.B620dev/% /sps/ams/lpsc/tree/Lx3.9/mc/protons.B620dev/%"
